#!/usr/bin/env bash

VERSION="0.0.14"

# shellcheck disable=SC2162
read -p "Please enter your Nuget API Key: "  apiKey

dotnet build -c release
dotnet pack -c release

dotnet nuget push --source https://nexus.tiki-dsp.io/repository/tiki-nuget/ --api-key "$apiKey" ai-common-dotnetcore/bin/Release/ai-common-dotnetcore."$VERSION".nupkg