pipeline {
    agent { kubernetes { label 'dsp-dotnetcore-31' } }

    options {
        // do not clean TAG build logs
        buildDiscarder(env.TAG_NAME == null ? logRotator(daysToKeepStr: '30') : null)
        // ensures that automated branch indexing doesn't trigger branch job builds,
        // but also destroys the its feature to trigger on bitbucket commit push
        // solution: additional bitbucketPush trigger below
        overrideIndexTriggers(false)
    }

    triggers {
        // ensure that the branch job itself gets triggered on bitbucket commit push
        bitbucketPush()
    }

    stages {
        stage('Prepare') {
            steps {
                bitbucketStatusNotify(buildState: 'INPROGRESS')
            }
        }    
        stage('Build') {
            steps {
                container('dotnetcore31') {
                    sh "dotnet build"
                }
            }
        }
        stage('Test') {
            steps {
                container('dotnetcore31') {
                    sh "dotnet test -l trx || exit 0"
                    mstest testResultsFile:"**/*.trx", keepLongStdio: true
                }
            }
        }
    }
    post {
        failure {
            notifyViaSlack("#FF0000")
            bitbucketStatusNotify(buildState: 'FAILED')
        }
        fixed {
            notifyViaSlack("#00FF00")
            bitbucketStatusNotify(buildState: 'SUCCESSFUL')
        }
    }
}

def notifyViaSlack(String colorCode) {
  def subject = "${currentBuild.currentResult}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'"
  def summary = "${subject} ${env.BUILD_URL}"

  slackSend (color: colorCode, message: summary)
}
