# To build, pack and push within a single step

run the [publish.sh](publish.sh) script	

# To build (and pack a nuget package)
## cd into ai-common-dotnetcore project dir and execute
`
dotnet build -c release
`

## then pack it by executing
`
dotnet pack -c release
`


# To push the nuget package to the repo use
dotnet nuget push --source https://nexus.tiki-dsp.io/repository/tiki-nuget/ --api-key <FROM KEY PASS>  bin\Release\ai-common-dotnetcore.0.0.5.1.nupkg
