using System;
using AiCommon.Common.ApplicationStartup;
using AiCommon.Common.ApplicationStartup.Initializers;
using AiCommon.Main.Web.Auth;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;

namespace AiCommon.Tests.Main.Web
{
    
    
    public class AuthConfigurationTest
    {

        [Test]
        public void TestDefaultAuthConfig()
        {

            var sc = new ServiceCollection();
            sc
                .UseApplicationServiceInitializer<SerilogInitializer>()
                .ConfigureJwtAuthentication();
        }

        [Test]
        public void TestOfCustomAuthConfigWithWrongServiceConfiguration()
        {

            var sc = new ServiceCollection();

            sc
                .UseApplicationServiceInitializer<SerilogInitializer>()
                .AddSingleton<IJwtBearerOptionsProvider, LocalJwtConfigurationWithWrongServices>();

            try
            {
                sc.ConfigureJwtAuthentication();
            }
            catch (NotSupportedException e)
            {
                Assert.AreEqual("It seems you customized JwtBearerOptions but did not installed a handler for type [AiCommon.Common.Auth.IJwtIssuerContext] which is mandatory.", e.Message);
            }         
            
        }
        class LocalJwtConfigurationWithWrongServices : IJwtBearerOptionsProvider
        {
            public void ConfigureServices(IServiceCollection serviceCollection)
            {
            }

            public void Configure(JwtBearerOptions options)
            {
            }
        }

    }
}