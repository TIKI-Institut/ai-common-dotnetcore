using System;
using System.Collections.Generic;
using System.Linq;
using AiCommon.Common.Kubernetes;
using AiCommon.Main.Qos;
using AiCommon.Main.Qos.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging.Abstractions;
using NUnit.Framework;

namespace AiCommon.Tests.Main.Qos
{
    public class SyncQosControllerTests
    {
        private class MySyncQosEntry : QosEntry
        {
            public MySyncQosEntry(string name)
            {
                Name = name;
                Type = QosType.Sync;

            }

            public sealed override string Name { get; set; }

            public override double Run()
            {
                throw new NotImplementedException();
            }
        }

        private class ReturningSyncEntry : QosEntry
        {
            public ReturningSyncEntry(string name)
            {
                Name = name;
                Type = QosType.Sync;
            }

            public sealed override string Name { get; set; }

            public override double Run()
            {
                return 0.4711;
            }
        }

        private class FailingSyncEntry : QosEntry
        {
            public FailingSyncEntry(string name)
            {
                Name = name;
                Type = QosType.Sync;
            }

            public sealed override string Name { get; set; }

            public override double Run()
            {
                throw new Exception("SOME FAILURE");
            }
        }
        
        [Test]
        public void List_WithoutEntries()
        {
            var testee = new SyncQosController(NullLogger<SyncQosController>.Instance, 
                new QosEntry[] {  }, 
                new CurrentFlavorReference(null, NullLogger<CurrentFlavorReference>.Instance), 
                new CurrentPodReference(null, NullLogger<CurrentPodReference>.Instance));

            var result = testee.List();

            Assert.IsInstanceOf<OkObjectResult>(result);
            var typedResult = (QosEndpoint) ((OkObjectResult) result).Value;
            Assert.IsInstanceOf<QosEndpoint>(typedResult);
              
            Assert.AreEqual(0,typedResult.Entries.Count);
        }

        [Test]
        public void List_WithEntries()
        {
            var testee = new SyncQosController(
                NullLogger<SyncQosController>.Instance, 
                new QosEntry[] {
                new MySyncQosEntry("A"),
                new MySyncQosEntry("B"),
                new MySyncQosEntry("C")
                },
                new CurrentFlavorReference(null, NullLogger<CurrentFlavorReference>.Instance), 
                new CurrentPodReference(null, NullLogger<CurrentPodReference>.Instance));

            var result = testee.List();

            Assert.IsInstanceOf<OkObjectResult>(result);
            var typedResult = (QosEndpoint) ((OkObjectResult) result).Value;
            Assert.IsInstanceOf<QosEndpoint>(typedResult);
            
            Assert.AreEqual(3,typedResult.Entries.Count());
            
            Assert.That(typedResult.Entries, Contains.Item(new MySyncQosEntry("A")).Using(new QosEntryComparer()));
            Assert.That(typedResult.Entries, Contains.Item(new MySyncQosEntry("B")).Using(new QosEntryComparer()));
            Assert.That(typedResult.Entries, Contains.Item(new MySyncQosEntry("C")).Using(new QosEntryComparer()));
        }

        [Test]
        public void Run_UnknownEntry()
        {
            var testee = new SyncQosController(NullLogger<SyncQosController>.Instance, new QosEntry[]
            {
                new MySyncQosEntry("A"),
                new MySyncQosEntry("B"),
                new MySyncQosEntry("C")
            }, 
                new CurrentFlavorReference(null, NullLogger<CurrentFlavorReference>.Instance),
                new CurrentPodReference(null, NullLogger<CurrentPodReference>.Instance));

            var result = testee.Run("1", "123");

            Assert.IsInstanceOf<OkObjectResult>(result);
            Assert.IsInstanceOf<QosRunResult>(((OkObjectResult) result).Value);
            
            Assert.AreEqual(QosRunStatus.NotFound,((QosRunResult) ((OkObjectResult) result).Value).Status);
            Assert.Null(((QosRunResult) ((OkObjectResult) result).Value).Score);
            Assert.True(((QosRunResult) ((OkObjectResult) result).Value).StatusMessage
                .StartsWith("Sync QoS entry '123' was not found"));
        }

        [Test]
        public void Run_KnownEntry()
        {
            var testee = new SyncQosController(NullLogger<SyncQosController>.Instance, new QosEntry[]
            {
                new MySyncQosEntry("A"),
                new ReturningSyncEntry("B"),
                new MySyncQosEntry("C")
            }, 
                new CurrentFlavorReference(null, NullLogger<CurrentFlavorReference>.Instance), 
                new CurrentPodReference(null, NullLogger<CurrentPodReference>.Instance));

            var result = testee.Run("1", "B");

            Assert.IsInstanceOf<OkObjectResult>(result);
            Assert.IsInstanceOf<QosRunResult>(((OkObjectResult) result).Value);
            
            Assert.AreEqual(QosRunStatus.Success,((QosRunResult) ((OkObjectResult) result).Value).Status);
            Assert.AreEqual(0.4711,((QosRunResult) ((OkObjectResult) result).Value).Score);
            Assert.True(((QosRunResult) ((OkObjectResult) result).Value).StatusMessage
                .StartsWith("Sync QoS run for B completed"));
        }
        
        [Test]
        public void Run_KnownEntry_WithError()
        {
            var testee = new SyncQosController(NullLogger<SyncQosController>.Instance, new QosEntry[]
            {
                new MySyncQosEntry("A"),
                new FailingSyncEntry("B"),
                new MySyncQosEntry("C")
            }, 
                new CurrentFlavorReference(null, NullLogger<CurrentFlavorReference>.Instance),
                new CurrentPodReference(null, NullLogger<CurrentPodReference>.Instance));

            var result = testee.Run("1","B");

            Assert.IsInstanceOf<OkObjectResult>(result);
            Assert.IsInstanceOf<QosRunResult>(((OkObjectResult) result).Value);
            
            Assert.AreEqual(QosRunStatus.Failed,((QosRunResult) ((OkObjectResult) result).Value).Status);
            Assert.Null(((QosRunResult) ((OkObjectResult) result).Value).Score);
            
            
            Assert.True( ((QosRunResult) ((OkObjectResult) result).Value).StatusMessage.Contains("System.Exception: Running sync QoS for [id:1] 'B' has failed"));
            Assert.True( ((QosRunResult) ((OkObjectResult) result).Value).StatusMessage.Contains("System.Exception: SOME FAILURE"));
        }
        
    }
}