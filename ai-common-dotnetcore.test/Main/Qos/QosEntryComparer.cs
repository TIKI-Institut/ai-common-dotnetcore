using System;
using System.Collections.Generic;
using AiCommon.Main.Qos.Model;

namespace AiCommon.Tests.Main.Qos
{
    public class QosEntryComparer : IComparer<QosEntry>
    {
        public int Compare(QosEntry x, QosEntry y)
        {
            var i = 0;

            if (y != null && (x != null && (i = (int) x.Type - (int) y.Type) != 0)) return i;
            return (i = string.Compare(x?.Name, y?.Name, StringComparison.Ordinal)) != 0 ? i : 0;
        }
    }
}