using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AiCommon.Common.Kubernetes;
using AiCommon.Common.Mqtt;
using AiCommon.Main.Qos;
using AiCommon.Main.Qos.Model;
using AiCommon.Tests.Common.Mqtt;
using Microsoft.Extensions.Logging.Abstractions;
using MQTTnet.Protocol;
using NUnit.Framework;

namespace AiCommon.Tests.Main.Qos
{
    public class AsyncQosJobTests
    {
        [Test]
        public void TestExecuteUnknownQosEntry()
        {
            var mqttClient = new TestMqttClient();
            var flavor = new MyFlavor();

            var testee = new AsyncQosJob(
                NullLogger<AsyncQosJob>.Instance,
                new QosEntry[] { },
                new LocalNamespaceBrokerClientProvider(mqttClient),
                flavor);

            var result = testee.Run(new AsyncQosJobOptions {TransactionId = "1", Name = "UNKNOWN"});

            Assert.AreEqual(0, result);
            Assert.AreEqual(1, mqttClient.Publications.Count);
            Assert.AreEqual("qos/result/async", mqttClient.Publications[0].Item1);
            Assert.AreEqual(MqttQualityOfServiceLevel.AtLeastOnce, mqttClient.Publications[0].Item2);

            Assert.True(mqttClient.Publications[0].Item3 is QosRunResult);

            Assert.Null(((QosRunResult) mqttClient.Publications[0].Item3).QosEntry);
            Assert.Null(((QosRunResult) mqttClient.Publications[0].Item3).Score);
            Assert.AreEqual(QosRunStatus.NotFound, ((QosRunResult) mqttClient.Publications[0].Item3).Status);
            Assert.AreEqual("Async QoS entry 'UNKNOWN' was not found",
                ((QosRunResult) mqttClient.Publications[0].Item3).StatusMessage);

            Assert.That(((QosRunResult) mqttClient.Publications[0].Item3).FlavorReference as IFlavorReference,
                Is.EqualTo(flavor).Using(new FlavorEqualComparer()));
            mqttClient.AssertNoSubscriptionsLeft();
        }

        [Test]
        public void TestExecuteKnownQosEntryWithSingleEntry()
        {
            var mqttClient = new TestMqttClient();
            var flavor = new MyFlavor();

            var testee = new AsyncQosJob(
                NullLogger<AsyncQosJob>.Instance,
                new QosEntry[] {new ScoreReturningQosEntry("WITHSCORE")},
                new LocalNamespaceBrokerClientProvider(mqttClient), flavor);


            var result = testee.Run(new AsyncQosJobOptions {TransactionId = "1", Name = "WITHSCORE"});

            Assert.AreEqual(0, result);
            Assert.AreEqual(1, mqttClient.Publications.Count);
            Assert.AreEqual("qos/result/async", mqttClient.Publications[0].Item1);
            Assert.AreEqual(MqttQualityOfServiceLevel.AtLeastOnce, mqttClient.Publications[0].Item2);

            Assert.True(mqttClient.Publications[0].Item3 is QosRunResult);

            Assert.AreEqual("WITHSCORE", ((QosRunResult) mqttClient.Publications[0].Item3).QosEntry.Name);
            Assert.AreEqual(0.4711, ((QosRunResult) mqttClient.Publications[0].Item3).Score);
            Assert.AreEqual(QosRunStatus.Success, ((QosRunResult) mqttClient.Publications[0].Item3).Status);
            Assert.True(((QosRunResult) mqttClient.Publications[0].Item3).StatusMessage
                .StartsWith("Async QoS run for WITHSCORE completed"));

            Assert.That(((QosRunResult) mqttClient.Publications[0].Item3).FlavorReference as IFlavorReference,
                Is.EqualTo(flavor).Using(new FlavorEqualComparer()));
            mqttClient.AssertNoSubscriptionsLeft();
        }

        [Test]
        public void TestExecuteKnownQosEntryWithMultipleEntries()
        {
            var mqttClient = new TestMqttClient();
            var flavor = new MyFlavor();

            var testee = new AsyncQosJob(
                NullLogger<AsyncQosJob>.Instance,
                new QosEntry[]
                {
                    new ScoreReturningQosEntry("_SOME_ENTRY_1"),
                    new ScoreReturningQosEntry("_SOME_ENTRY_2"),
                    new ScoreReturningQosEntry("WITHSCORE"),
                    new ScoreReturningQosEntry("_SOME_ENTRY_3")
                },
                new LocalNamespaceBrokerClientProvider(mqttClient), flavor);


            var result = testee.Run(new AsyncQosJobOptions {TransactionId = "1", Name = "WITHSCORE"});

            Assert.AreEqual(0, result);
            Assert.AreEqual(1, mqttClient.Publications.Count);
            Assert.AreEqual("qos/result/async", mqttClient.Publications[0].Item1);
            Assert.AreEqual(MqttQualityOfServiceLevel.AtLeastOnce, mqttClient.Publications[0].Item2);

            Assert.True(mqttClient.Publications[0].Item3 is QosRunResult);

            Assert.AreEqual("WITHSCORE", ((QosRunResult) mqttClient.Publications[0].Item3).QosEntry.Name);
            Assert.AreEqual(0.4711, ((QosRunResult) mqttClient.Publications[0].Item3).Score);
            Assert.AreEqual(QosRunStatus.Success, ((QosRunResult) mqttClient.Publications[0].Item3).Status);
            Assert.True(((QosRunResult) mqttClient.Publications[0].Item3).StatusMessage
                .StartsWith("Async QoS run for WITHSCORE completed"));

            Assert.That(((QosRunResult) mqttClient.Publications[0].Item3).FlavorReference as IFlavorReference,
                Is.EqualTo(flavor).Using(new FlavorEqualComparer()));
            mqttClient.AssertNoSubscriptionsLeft();
        }

        [Test]
        public void TestExecuteFailingQosEntryWithMultipleEntries()
        {
            var mqttClient = new TestMqttClient();
            var flavor = new MyFlavor();

            var testee = new AsyncQosJob(
                NullLogger<AsyncQosJob>.Instance,
                new QosEntry[]
                {
                    new ScoreReturningQosEntry("_SOME_ENTRY_1"),
                    new ScoreReturningQosEntry("_SOME_ENTRY_2"),
                    new ScoreReturningQosEntry("WITHSCORE", true),
                    new ScoreReturningQosEntry("_SOME_ENTRY_3")
                },
                new LocalNamespaceBrokerClientProvider(mqttClient), flavor);

            var result = testee.Run(new AsyncQosJobOptions {TransactionId = "1", Name = "WITHSCORE"});

            Assert.AreEqual(0, result);
            Assert.AreEqual(1, mqttClient.Publications.Count);
            Assert.AreEqual("qos/result/async", mqttClient.Publications[0].Item1);
            Assert.AreEqual(MqttQualityOfServiceLevel.AtLeastOnce, mqttClient.Publications[0].Item2);

            Assert.True(mqttClient.Publications[0].Item3 is QosRunResult);

            Assert.AreEqual("WITHSCORE", ((QosRunResult) mqttClient.Publications[0].Item3).QosEntry.Name);
            Assert.Null(((QosRunResult) mqttClient.Publications[0].Item3).Score);
            Assert.AreEqual(QosRunStatus.Failed, ((QosRunResult) mqttClient.Publications[0].Item3).Status);

            Assert.NotNull(((QosRunResult) mqttClient.Publications[0].Item3).StatusMessage);

            Assert.True(((QosRunResult) mqttClient.Publications[0].Item3).StatusMessage
                .Contains("System.Exception: Running async QoS for [id:1] 'WITHSCORE' has failed",
                    StringComparison.InvariantCultureIgnoreCase));
            Assert.True(((QosRunResult) mqttClient.Publications[0].Item3).StatusMessage
                .Contains("System.Exception: SOME FAILURE", StringComparison.InvariantCultureIgnoreCase));

            Assert.That(((QosRunResult) mqttClient.Publications[0].Item3).FlavorReference as IFlavorReference,
                Is.EqualTo(flavor).Using(new FlavorEqualComparer()));
            mqttClient.AssertNoSubscriptionsLeft();
        }

        [Test]
        public void TestExecuteFailingBecauseOfTransmissionError()
        {
            var mqttClient = new FailingTransmissionMqttClient(true);
            var flavor = new MyFlavor();

            var testee = new AsyncQosJob(
                NullLogger<AsyncQosJob>.Instance,
                new QosEntry[] {new ScoreReturningQosEntry("WITHSCORE")},
                new LocalNamespaceBrokerClientProvider(mqttClient), flavor);

            var result = testee.Run(new AsyncQosJobOptions {TransactionId = "1", Name = "WITHSCORE"});

            Assert.AreEqual(1, result);
            Assert.AreEqual(0, mqttClient.Publications.Count);

            mqttClient.AssertNoSubscriptionsLeft();
        }

        [Test]
        public void TestExecuteWithConnectionDelay()
        {
            var mqttClient = new FailingTransmissionMqttClient(false, TimeSpan.FromSeconds(2));
            var flavor = new MyFlavor();

            var testee = new AsyncQosJob(
                NullLogger<AsyncQosJob>.Instance,
                new QosEntry[] {new ScoreReturningQosEntry("WITHSCORE")},
                new LocalNamespaceBrokerClientProvider(mqttClient), flavor);

            var result = testee.Run(new AsyncQosJobOptions {TransactionId = "1", Name = "WITHSCORE"});

            Assert.AreEqual(0, result);
            Assert.AreEqual(1, mqttClient.Publications.Count);

            mqttClient.AssertNoSubscriptionsLeft();
        }

        private class FlavorEqualComparer : IComparer<IFlavorReference>
        {
            public int Compare(IFlavorReference x, IFlavorReference y)
            {
                int i;
                if ((i = string.Compare(x?.Principal, y?.Principal, StringComparison.Ordinal)) != 0) return i;
                if ((i = string.Compare(x?.Name, y?.Name, StringComparison.Ordinal)) != 0) return i;
                if ((i = string.Compare(x?.Environment, y?.Environment, StringComparison.Ordinal)) != 0) return i;
                if ((i = string.Compare(x?.FlavorName, y?.FlavorName, StringComparison.Ordinal)) != 0) return i;
                return (i = string.Compare(x?.FlavorVersion, y?.FlavorVersion, StringComparison.Ordinal)) != 0 ? i : 0;
            }
        }

        private class LocalNamespaceBrokerClientProvider : ILocalNamespaceBrokerClientProvider
        {
            private readonly TestMqttClient _targetClient;

            public LocalNamespaceBrokerClientProvider(TestMqttClient targetClient)
            {
                _targetClient = targetClient;
            }

            public IMqttClient CreateClient(string clientIdSuffix)
            {
                Assert.False(string.IsNullOrWhiteSpace(clientIdSuffix));
                return _targetClient;
            }
        }

        private class MyFlavor : ICurrentFlavorReference
        {
            public string Principal => "Principal";
            public string Name => "Name";
            public string Environment => "Environment";
            public string FlavorName => "FlavorName";
            public string FlavorVersion => "FlavorVersion";
        }

        private class ScoreReturningQosEntry : QosEntry
        {
            private readonly bool _fail;

            public ScoreReturningQosEntry(string name, bool fail = false)
            {
                Name = name;
                _fail = fail;
                Type = QosType.Async;
            }

            public sealed override string Name { get; set; }

            public override double Run()
            {
                if (_fail)
                    throw new Exception("SOME FAILURE");
                return 0.4711;
            }
        }

        private class FailingTransmissionMqttClient : TestMqttClient
        {
            private readonly TimeSpan _connectionDelay;
            private readonly bool _transmissionShallFail;

            public FailingTransmissionMqttClient(bool transmissionShallFail, TimeSpan connectionDelay)
            {
                _transmissionShallFail = transmissionShallFail;
                _connectionDelay = connectionDelay;
            }

            public FailingTransmissionMqttClient(bool transmissionShallFail) : this(transmissionShallFail,
                TimeSpan.Zero)
            {
            }

            public override Task Connect()
            {
                Task.Delay(_connectionDelay).Wait();
                return base.Connect();
            }

            public override Task Publish<TObject>(string topic, TObject payload, MqttQualityOfServiceLevel qosLevel)
            {
                if (_transmissionShallFail)
                    throw new Exception("SOME TRANSMISSION ERROR");
                return base.Publish(topic, payload, qosLevel);
            }
        }
    }
}