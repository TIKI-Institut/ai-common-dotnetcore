using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AiCommon.Common.Kubernetes;
using AiCommon.Common.Mqtt;
using AiCommon.Main.Qos;
using AiCommon.Main.Qos.Model;
using AiCommon.Tests.Common.Mqtt;
using Microsoft.Extensions.Logging.Abstractions;
using MQTTnet.Protocol;
using NUnit.Framework;

namespace AiCommon.Tests.Main.Qos
{
    public class AsyncQosRegisterJobTests
    {
        private class FlavorEqualComparer :IComparer<IFlavorReference>
        {
            public int Compare(IFlavorReference x, IFlavorReference y)
            {
                int i;
                if ((i = string.Compare(x?.Principal, y?.Principal, StringComparison.Ordinal)) != 0) return i;
                if ((i = string.Compare(x?.Name, y?.Name, StringComparison.Ordinal)) != 0) return i;
                if ((i = string.Compare(x?.Environment, y?.Environment, StringComparison.Ordinal)) != 0) return i;
                if ((i = string.Compare(x?.FlavorName, y?.FlavorName, StringComparison.Ordinal)) != 0) return i;
                return (i = string.Compare(x?.FlavorVersion, y?.FlavorVersion, StringComparison.Ordinal)) != 0 ? i : 0;
            }
        }

        private class LocalNamespaceBrokerClientProvider : ILocalNamespaceBrokerClientProvider
        {
            private readonly TestMqttClient _targetClient;

            public LocalNamespaceBrokerClientProvider(TestMqttClient targetClient)
            {
                _targetClient = targetClient;
            }

            public IMqttClient CreateClient(string clientIdSuffix)
            {
                Assert.False(string.IsNullOrWhiteSpace(clientIdSuffix));
                return _targetClient;  
            }
        }

        private class MyFlavor : ICurrentFlavorReference
        {
            public string Principal => "Principal";
            public string Name => "Name";
            public string Environment => "Environment";
            public string FlavorName => "FlavorName";
            public string FlavorVersion => "FlavorVersion";
        }

        private class MyAsyncQosEntry : QosEntry
        {
            public MyAsyncQosEntry(string name)
            {
                Name = name;
                Type = QosType.Async;
            }

            public sealed override string Name { get; set; }

            public override double Run()
            {
                throw new NotImplementedException();
            }
        }

        private class FailingTransmissionMqttClient : TestMqttClient
        {
            private readonly bool _transmissionShallFail;
            private readonly TimeSpan _connectionDelay;

            public FailingTransmissionMqttClient(bool transmissionShallFail, TimeSpan connectionDelay)
            {
                _transmissionShallFail = transmissionShallFail;
                _connectionDelay = connectionDelay;
            }

            public FailingTransmissionMqttClient(bool transmissionShallFail) : this(transmissionShallFail, TimeSpan.Zero)
            {
            }

            public override Task Connect()
            {
                Task.Delay(_connectionDelay).Wait();
                return base.Connect();
            }

            public override Task Publish<TObject>(string topic, TObject payload, MqttQualityOfServiceLevel qosLevel)
            {
                if (_transmissionShallFail)
                    throw new Exception("SOME TRANSMISSION ERROR");
                return base.Publish(topic, payload, qosLevel);
            }
        }
        
        [Test]
        public void TestQosRegisterNoEntries()
        {

            var mqttClient = new TestMqttClient();
            var flavor = new MyFlavor();  
            
            var testee = new AsyncQosRegisterJob(
                NullLogger<AsyncQosRegisterJob>.Instance, 
                new QosEntry[] { }, 
                new LocalNamespaceBrokerClientProvider(mqttClient), 
                flavor);

            var result = testee.Run(new AsyncQosRegisterOptions( ));

            Assert.AreEqual(0, result);
            Assert.AreEqual(0, mqttClient.Publications.Count);
        }
        
        [Test]
        public void TestQosRegisterEntries()
        {

            var mqttClient = new TestMqttClient();
            var flavor = new MyFlavor();  
            
            var testee = new AsyncQosRegisterJob(
                NullLogger<AsyncQosRegisterJob>.Instance, 
                new QosEntry[]
                {
                    new MyAsyncQosEntry("_SOME_ENTRY_1") ,
                    new MyAsyncQosEntry("_SOME_ENTRY_2") ,
                    new MyAsyncQosEntry("WITHSCORE"), 
                    new MyAsyncQosEntry("_SOME_ENTRY_3")
                }, 
                new LocalNamespaceBrokerClientProvider(mqttClient), 
                flavor);

            var result = testee.Run(new AsyncQosRegisterOptions( ));

            Assert.AreEqual(0, result);
            Assert.AreEqual(1, mqttClient.Publications.Count);
            
            Assert.AreEqual("qos/register/async", mqttClient.Publications[0].Item1);
            Assert.AreEqual(MqttQualityOfServiceLevel.AtLeastOnce, mqttClient.Publications[0].Item2);
            
            Assert.True(mqttClient.Publications[0].Item3 is QosEndpoint);

            Assert.That(((QosEndpoint) mqttClient.Publications[0].Item3).FlavorReference as IFlavorReference,Is.EqualTo(flavor).Using(new FlavorEqualComparer()));

            Assert.AreEqual(4, ((QosEndpoint) mqttClient.Publications[0].Item3).Entries.Count);
            
            Assert.That(((QosEndpoint) mqttClient.Publications[0].Item3).Entries, Contains.Item(new MyAsyncQosEntry("_SOME_ENTRY_1")).Using(new QosEntryComparer()));
            Assert.That(((QosEndpoint) mqttClient.Publications[0].Item3).Entries, Contains.Item(new MyAsyncQosEntry("WITHSCORE")).Using(new QosEntryComparer()));
            Assert.That(((QosEndpoint) mqttClient.Publications[0].Item3).Entries, Contains.Item(new MyAsyncQosEntry("_SOME_ENTRY_2")).Using(new QosEntryComparer()));
            Assert.That(((QosEndpoint) mqttClient.Publications[0].Item3).Entries, Contains.Item(new MyAsyncQosEntry("_SOME_ENTRY_3")).Using(new QosEntryComparer()));
            
            mqttClient.AssertNoSubscriptionsLeft();


        }
        
        [Test]
        public void TestQosRegisterEntriesFailingBecauseOfTransmissionError()
        {

            var mqttClient = new FailingTransmissionMqttClient(true);
            var flavor = new MyFlavor();  
            
            var testee = new AsyncQosRegisterJob(
                NullLogger<AsyncQosRegisterJob>.Instance, 
                new QosEntry[] { new MyAsyncQosEntry("_SOME_ENTRY_1") }, 
                new LocalNamespaceBrokerClientProvider(mqttClient), flavor);

            var result = testee.Run(new AsyncQosRegisterOptions ());

            Assert.AreEqual(1, result);
            Assert.AreEqual(0, mqttClient.Publications.Count);
            
            mqttClient.AssertNoSubscriptionsLeft();
        }
    }
}