using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AiCommon.Common.Mqtt;
using MQTTnet.Protocol;
using NUnit.Framework;

namespace AiCommon.Tests.Common.Mqtt
{
    public class TestMqttClient : IMqttClient
    {
        public interface ISubscriptionScheduler
        {
            Task Trigger(string topic, object payload, Exception e);
        }
        
        private class PayloadScheduler<TPayload> : ISubscriptionScheduler where TPayload : class
        {
            private readonly Func<string, TPayload, Exception, Task> _handler;

            public PayloadScheduler(Func<string, TPayload, Exception, Task> handler)
            {
                _handler = handler;
            }


            public Task Trigger(string topic, object payload, Exception e)
            {
                if (!(payload is TPayload))
                    throw new ArgumentException("incompatible argument type", nameof(payload));

                return _handler(topic, (TPayload)payload, e);
            }
        }

        private class Scheduler : ISubscriptionScheduler 
        {
            private readonly Func<string, Exception, Task> _handler;

            public Scheduler(Func<string, Exception, Task> handler)
            {
                _handler = handler;
            }


            public Task Trigger(string topic, object payload, Exception e)
            {
                if (payload != null)
                    throw new ArgumentException("incompatible argument type", nameof(payload));

                return _handler(topic, e);
            }
        }

        public  Dictionary<Guid, (string,MqttQualityOfServiceLevel, ISubscriptionScheduler)> Subscriptions = new Dictionary<Guid, (string,MqttQualityOfServiceLevel, ISubscriptionScheduler)>();
        public  List<Guid> Unsubscriptions = new List<Guid>();
        public List<(string, MqttQualityOfServiceLevel, object)> Publications = new List<(string, MqttQualityOfServiceLevel, object)>();
        
        public virtual Guid Subscribe<TPayload>(string topic, MqttQualityOfServiceLevel qos, Func<string, TPayload, Exception, Task> handler) where TPayload : class
        {
            var key = Guid.NewGuid();
            Subscriptions[key] = (topic, qos, new PayloadScheduler<TPayload>(handler));
            return key;
        }

        public virtual Guid Subscribe(string topic, MqttQualityOfServiceLevel qos, Func<string, Exception, Task> handler)
        {
            var key = Guid.NewGuid();
            Subscriptions[key] = (topic, qos, new Scheduler(handler));
            return key;
        }

        public virtual void Unsubscribe(Guid subscriptionId)
        {
            Unsubscriptions.Add(subscriptionId);
        }

        public string ClientId() => "TestMqttClient";

        public virtual Task Connect()
        {
            return Task.CompletedTask;
        }

        public virtual Task Disconnect()
        {
            return Task.CompletedTask;
        }

        public virtual Task Publish<TObject>(string topic, TObject payload, MqttQualityOfServiceLevel qosLevel)
        {
            Publications.Add((topic, qosLevel, payload));
            return Task.CompletedTask;
        }

        public virtual Task Publish(string topic, string payload, MqttQualityOfServiceLevel qosLevel)
        {
            Publications.Add((topic, qosLevel, payload));
            return Task.CompletedTask;
        }

        public virtual Task Publish(string topic, MqttQualityOfServiceLevel qosLevel)
        {
            Publications.Add((topic, qosLevel, null));
            return Task.CompletedTask;
        }

        public void AssertNoSubscriptionsLeft()
        {
            Assert.AreEqual(Unsubscriptions.Count, Subscriptions.Count);
            Assert.True(Unsubscriptions.All(Subscriptions.ContainsKey));
        }
    }
}