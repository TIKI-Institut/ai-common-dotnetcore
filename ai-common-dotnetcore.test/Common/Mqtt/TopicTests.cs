using System;
using AiCommon.Common.Mqtt;
using NUnit.Framework;

namespace AiCommon.Tests.Common.Mqtt
{
    public class TopicTests
    {
        [Test]
        [TestCaseSource(nameof(ValidTopics))]
        public void TestValidTopic(string topic)
        {
            // ReSharper disable once ObjectCreationAsStatement
            Assert.DoesNotThrow(() => new Topic(topic));
        }


        static readonly object[] ValidTopics =
        {
            new object[] {"house/room/main-light"},
            new object[] {"house/room/side-light"},
            new object[] {"house/+/side-light"},
            new object[] {"house/#"},
            new object[] {"house/room/#"},
            new object[] {"house/+/+"},
            new object[] {"+/+/+"},
        };


        [Test]
        [TestCaseSource(nameof(InvalidTopics))]
        public void TestInvalidTopic(string topic)
        {
            // ReSharper disable once ObjectCreationAsStatement
            Assert.Throws<ArgumentException>(() => new Topic(topic));
        }


        static readonly object[] InvalidTopics =
        {
            new object[] {"#/"},
            new object[] {"house/#/main-light"},
            new object[] {"house//side-light"}
        };

        [Test]
        [TestCaseSource(nameof(MatchedSubscriptions))]
        public void TestMatchingSubscription(string a, string b, bool aCoversB, bool bCoversA )
        {
            var s = new Topic(a);
            var r = new Topic(b);

            Assert.AreEqual(aCoversB, s.Covers(r));
            Assert.AreEqual(bCoversA, r.Covers(s));
        }


        static readonly object[] MatchedSubscriptions =
        {
            new object[] {"myhome/groundfloor/+/temperature", "myhome/groundfloor/livingroom/temperature", true, false},
            new object[] {"myhome/groundfloor/+/temperature", "myhome/groundfloor/kitchen/temperature", true, false},
            new object[] {"myhome/groundfloor/#", "myhome/groundfloor/livingroom/temperature", true, false},
            new object[] {"myhome/groundfloor/#", "myhome/groundfloor/kitchen/temperature", true, false},
            new object[] {"myhome/groundfloor/#", "myhome/groundfloor/kitchen/brightness", true, false},
            new object[] {"myhome/groundfloor/livingroom/temperature", "myhome/groundfloor/+/temperature", false, true},
            new object[] {"myhome/groundfloor/kitchen/temperature", "myhome/groundfloor/+/temperature", false, true,},
            new object[] {"myhome/groundfloor/livingroom/temperature", "myhome/groundfloor/#", false, true},
            new object[] {"myhome/groundfloor/kitchen/temperature", "myhome/groundfloor/#", false, true},
            new object[] {"myhome/groundfloor/kitchen/brightness", "myhome/groundfloor/#", false, true},
            new object[] {"house/#", "house/room1/main-light", true, false},
            new object[] {"house/#", "house/room1/alarm", true, false},
            new object[] {"house/#", "house/garage/main-light", true, false},
            new object[] {"house/#", "house/main-door", true, false},
            new object[] {"house/+/main-light", "house/room1/main-light", true, false},
            new object[] {"house/+/main-light", "house/room2/main-light", true, false},
            new object[] {"house/+/main-light", "house/garage/main-light", true, false},
            new object[] {"myhome/groundfloor/room1/temperature", "myhome/groundfloor/room1/temperature", true, true},
            new object[] {"myhome/groundfloor/room2/temperature", "myhome/groundfloor/room2/temperature", true, true},
        };

        [Test]
        [TestCaseSource(nameof(NonMatchedSubscriptions))]
        public void TestNonMatchingSubscription(string subscriptionTopic, string receivingTopic)
        {
            var s = new Topic(subscriptionTopic);
            var r = new Topic(receivingTopic);
            Assert.False(s.Covers(r));
        }


        static readonly object[] NonMatchedSubscriptions =
        {
            new object[] {"myhome/groundfloor/+/temperature", "myhome/firstfloor/kitchen/temperature"},
            new object[] {"myhome/groundfloor/+/temperature", "myhome/groundfloor/kitchen/brightness"},
            new object[] {"myhome/groundfloor/+/temperature", "myhome/groundfloor/kitchen/fridge/temperature"},
            new object[] {"myhome/groundfloor/#", "myhome/firstfloor/kitchen/temperature"},
            new object[] {"house/+/main-light", "house/room1/side-light"},
            new object[] {"house/+/main-light", "house/room2/side-light"},
            new object[] {"myhome/groundfloor/room1/temperature", "myhome/groundfloor/room2/temperature"},
            new object[] {"myhome/groundfloor/room2/temperature", "myhome/groundfloor/room1/temperature"},
        };
    }
}