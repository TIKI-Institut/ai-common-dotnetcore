using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AiCommon.Common.Mqtt;
using Microsoft.Extensions.Logging.Abstractions;
using MQTTnet;
using MQTTnet.Client.Connecting;
using MQTTnet.Client.Disconnecting;
using MQTTnet.Client.Publishing;
using MQTTnet.Client.Receiving;
using MQTTnet.Extensions.ManagedClient;
using MQTTnet.Protocol;
using Newtonsoft.Json;
using NUnit.Framework;
using IMqttClient = MQTTnet.Client.IMqttClient;

// ReSharper disable NonReadonlyMemberInGetHashCode

namespace AiCommon.Tests.Common.Mqtt
{
    public class ClientMessageReceiverTests
    {
        class ManagedConnectedClient : IManagedMqttClient
        {
            public List<MqttTopicFilter> Subscriptions = new List<MqttTopicFilter>();
            public List<string> Unsubscriptions = new List<string>();

            public IMqttApplicationMessageReceivedHandler ApplicationMessageReceivedHandler { get; set; }

            public Task<MqttClientPublishResult> PublishAsync(MqttApplicationMessage applicationMessage,
                CancellationToken cancellationToken)
            {
                throw new NotImplementedException();
            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public Task StartAsync(IManagedMqttClientOptions options)
            {
                throw new NotImplementedException();
            }

            public Task StopAsync()
            {
                throw new NotImplementedException();
            }

            public Task PingAsync(CancellationToken cancellationToken)
            {
                throw new NotImplementedException();
            }

            public Task SubscribeAsync(IEnumerable<MqttTopicFilter> topicFilters)
            {
                Subscriptions.AddRange(topicFilters);
                return Task.CompletedTask;
            }

            public Task UnsubscribeAsync(IEnumerable<string> topics)
            {
                Unsubscriptions.AddRange(topics);
                return Task.CompletedTask;
            }

            public Task PublishAsync(ManagedMqttApplicationMessage applicationMessages)
            {
                throw new NotImplementedException();
            }

            public IMqttClient InternalClient { get; }

            public bool IsStarted => true;
            public bool IsConnected => true;
            // ReSharper disable once UnassignedGetOnlyAutoProperty
            public int PendingApplicationMessagesCount { get; }
            // ReSharper disable once UnassignedGetOnlyAutoProperty
            public IManagedMqttClientOptions Options { get; }
            public IMqttClientConnectedHandler ConnectedHandler { get; set; }
            public IMqttClientDisconnectedHandler DisconnectedHandler { get; set; }
            public IApplicationMessageProcessedHandler ApplicationMessageProcessedHandler { get; set; }
            public IApplicationMessageSkippedHandler ApplicationMessageSkippedHandler { get; set; }
            public IConnectingFailedHandler ConnectingFailedHandler { get; set; }
            public ISynchronizingSubscriptionsFailedHandler SynchronizingSubscriptionsFailedHandler { get; set; }
        }

        class TopicFilterComparer : IComparer<MqttTopicFilter>
        {
            private int CompareNullable<T>(T? a, T? b) where T : struct
            {
                if (!a.HasValue && !b.HasValue)
                    return 0;

                if (a.HasValue && !b.HasValue)
                    return 1;

                if (!a.HasValue)
                    return -1;

                return Comparer<T>.Default.Compare(a.Value, b.Value);
            }

            public int Compare(MqttTopicFilter x, MqttTopicFilter y)
            {
                int t;

                if ((t = Comparer<string>.Default.Compare(x?.Topic, y?.Topic)) != 0) return t;
                if ((t = CompareNullable(x?.NoLocal, y?.NoLocal)) != 0) return t;
                if ((t = CompareNullable(x?.RetainHandling, y?.RetainHandling)) != 0) return t;
                if ((t = CompareNullable(x?.RetainAsPublished, y?.RetainAsPublished)) != 0) return t;
                return (t = CompareNullable(x?.QualityOfServiceLevel, y?.QualityOfServiceLevel)) != 0 ? t : 0;
            }
        }

        [JsonObject(MemberSerialization.OptOut)]
        public class TestMessagePayload
        {
            [JsonProperty("i", Required = Required.Always)] public int TestInt { get; set; }

            [JsonProperty("s")] public string TestString { get; set; }

            protected bool Equals(TestMessagePayload other)
            {
                return TestInt == other.TestInt && string.Equals(TestString, other.TestString);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != this.GetType()) return false;
                return Equals((TestMessagePayload) obj);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    return (TestInt * 397) ^ (TestString != null ? TestString.GetHashCode() : 0);
                }
            }
        }

        [Test]
        public void TestSubscription()
        {
            var client = new ManagedConnectedClient();
            var msgReceiver = new ClientMessageReceiver(client, NullLogger.Instance);

            var subId = msgReceiver.Subscribe<TestMessagePayload>(
                "topics/test",
                MqttQualityOfServiceLevel.ExactlyOnce,
                (topic, payload, e) =>
                    Task.CompletedTask);

            msgReceiver.Unsubscribe(subId);

            Assert.That(
                new MqttTopicFilter {Topic = "topics/test", QualityOfServiceLevel = MqttQualityOfServiceLevel.ExactlyOnce},
                Is.EqualTo(client.Subscriptions[0]).Using(new TopicFilterComparer()));
            Assert.AreEqual("topics/test", client.Unsubscriptions[0]);
        }

        [Test]
        public void TestMultipleSubscriptions()
        {
            var client = new ManagedConnectedClient();
            var msgReceiver = new ClientMessageReceiver(client, NullLogger.Instance);

            var subIdA = msgReceiver.Subscribe<TestMessagePayload>(
                "topics/test",
                MqttQualityOfServiceLevel.ExactlyOnce,
                (topic, payload, e) =>
                    Task.CompletedTask);

            var subIdB = msgReceiver.Subscribe<TestMessagePayload>(
                "topics/test",
                MqttQualityOfServiceLevel.AtMostOnce,
                (topic, payload, e) =>
                    Task.CompletedTask);

            msgReceiver.Unsubscribe(subIdA);
            msgReceiver.Unsubscribe(subIdB);

            Assert.That(
                new MqttTopicFilter {Topic = "topics/test", QualityOfServiceLevel = MqttQualityOfServiceLevel.ExactlyOnce},
                Is.EqualTo(client.Subscriptions[0]).Using(new TopicFilterComparer()));
            Assert.That(
                new MqttTopicFilter {Topic = "topics/test", QualityOfServiceLevel = MqttQualityOfServiceLevel.AtMostOnce},
                Is.EqualTo(client.Subscriptions[1]).Using(new TopicFilterComparer()));
            Assert.AreEqual("topics/test", client.Unsubscriptions[0]);
        }

        [Test]
        public void TestMultipleSubscriptionsOnDifferentTopics()
        {
            var client = new ManagedConnectedClient();
            var msgReceiver = new ClientMessageReceiver(client, NullLogger.Instance);

            var subIdA = msgReceiver.Subscribe<TestMessagePayload>(
                "topics/+/test",
                MqttQualityOfServiceLevel.ExactlyOnce,
                (topic, payload, e) =>
                    Task.CompletedTask);

            var subIdB = msgReceiver.Subscribe<TestMessagePayload>(
                "topics/#",
                MqttQualityOfServiceLevel.AtMostOnce,
                (topic, payload, e) =>
                    Task.CompletedTask);

            msgReceiver.Unsubscribe(subIdA);
            msgReceiver.Unsubscribe(subIdB);

            Assert.That(
                new MqttTopicFilter
                    {Topic = "topics/+/test", QualityOfServiceLevel = MqttQualityOfServiceLevel.ExactlyOnce},
                Is.EqualTo(client.Subscriptions[0]).Using(new TopicFilterComparer()));
            Assert.That(
                new MqttTopicFilter {Topic = "topics/#", QualityOfServiceLevel = MqttQualityOfServiceLevel.AtMostOnce},
                Is.EqualTo(client.Subscriptions[1]).Using(new TopicFilterComparer()));
            Assert.AreEqual("topics/+/test", client.Unsubscriptions[0]);
            Assert.AreEqual("topics/#", client.Unsubscriptions[1]);
        }

        [Test]
        public void TestMessageReceivingOnUniqueTopic()
        {
            var client = new ManagedConnectedClient();
            var msgReceiver = new ClientMessageReceiver(client, NullLogger.Instance);

            var receivedMessages = new List<TestMessagePayload>();

            var subIdA = msgReceiver.Subscribe<TestMessagePayload>(
                "topics/+/test",
                MqttQualityOfServiceLevel.ExactlyOnce,
                (topic, payload, e) =>
                {
                    receivedMessages.Add(payload);
                    return Task.CompletedTask;
                });

            var subIdB = msgReceiver.Subscribe<TestMessagePayload>(
                "topicsS/#",
                MqttQualityOfServiceLevel.AtMostOnce,
                (topic, payload, e) =>
                {
                    Assert.Fail("no message should be delivered here");
                    return Task.CompletedTask;
                }
            );


            msgReceiver.HandleApplicationMessageReceivedAsync(CreateMessageObject("topics/blub/test", 1, "blub"))
                .Wait();
            msgReceiver.HandleApplicationMessageReceivedAsync(CreateMessageObject("topics/blub2/test", 2, "foo"))
                .Wait();
            msgReceiver.HandleApplicationMessageReceivedAsync(CreateMessageObject("topics/blub3/test", 3, "bar"))
                .Wait();

            msgReceiver.Unsubscribe(subIdA);
            msgReceiver.Unsubscribe(subIdB);


            Assert.AreEqual(3, receivedMessages.Count);
            
            Assert.Contains(new TestMessagePayload {TestInt = 1, TestString = "blub"},receivedMessages);
            Assert.Contains(new TestMessagePayload {TestInt = 2, TestString = "foo"},receivedMessages);
            Assert.Contains(new TestMessagePayload {TestInt = 3, TestString = "bar"},receivedMessages);
        }

        [Test]
        public void TestStringMessageReceiving()
        {
            var client = new ManagedConnectedClient();
            var msgReceiver = new ClientMessageReceiver(client, NullLogger.Instance);

            var receivedMessages = new List<string>();

            var subscription = msgReceiver.Subscribe<string>(
                "topics/+/stringtest",
                MqttQualityOfServiceLevel.ExactlyOnce,
                (topic, payload, e) =>
                {
                    receivedMessages.Add(payload);
                    return Task.CompletedTask;
                });

            msgReceiver.HandleApplicationMessageReceivedAsync(
            new MqttApplicationMessageReceivedEventArgs("sender", new MqttApplicationMessage
                {
                    Topic = "topics/blub/stringtest",
                    Payload = Encoding.UTF8.GetBytes("\"blub\"")
                }, null, null)).Wait();

            msgReceiver.Unsubscribe(subscription);

            Assert.AreEqual(1, receivedMessages.Count);
            Assert.Contains("blub",receivedMessages);
        }

        [Test]
        public void TestMessageReceivingOnMultipleSubscriptions()
        {
            var client = new ManagedConnectedClient();
            var msgReceiver = new ClientMessageReceiver(client, NullLogger.Instance);

            var receivedMessages = new List<(int, string, TestMessagePayload, Exception)>();

            var subIdA = msgReceiver.Subscribe<TestMessagePayload>(
                "topics/+/test",
                MqttQualityOfServiceLevel.ExactlyOnce,
                (topic, payload, e) =>
                {
                    receivedMessages.Add((0, topic, payload, e));
                    return Task.CompletedTask;
                });

            var subIdB = msgReceiver.Subscribe<TestMessagePayload>(
                "topics/#",
                MqttQualityOfServiceLevel.AtMostOnce,
                (topic, payload, e) =>
                {
                    receivedMessages.Add((1, topic, payload, e));
                    return Task.CompletedTask;
                }
            );


            msgReceiver.HandleApplicationMessageReceivedAsync(CreateMessageObject("topics/blub/test", 1, "blub")).Wait();
            msgReceiver.HandleApplicationMessageReceivedAsync(CreateMessageObject("topics/blub2/tests", 2, "foo")).Wait();
            msgReceiver.HandleApplicationMessageReceivedAsync(CreateMessageObject("topics/blub3/test", 3, "bar")).Wait();

            msgReceiver.Unsubscribe(subIdA);
            msgReceiver.Unsubscribe(subIdB);


            Assert.AreEqual(5, receivedMessages.Count);

            var receiveingGroups = receivedMessages.GroupBy(x => x.Item1).ToDictionary(x => x.Key, x => x.ToList());
            
            Assert.AreEqual(2, receiveingGroups[0].Count());
            Assert.AreEqual(3, receiveingGroups[1].Count());
            
            Assert.Contains((0, "topics/blub/test", new TestMessagePayload {TestInt = 1, TestString = "blub"}, null as Exception),receiveingGroups[0]);
            Assert.Contains((0, "topics/blub3/test", new TestMessagePayload {TestInt = 3, TestString = "bar"}, null as Exception),receiveingGroups[0]);

            Assert.Contains((1, "topics/blub/test", new TestMessagePayload {TestInt = 1, TestString = "blub"}, null as Exception),receiveingGroups[1]);
            Assert.Contains((1, "topics/blub2/tests", new TestMessagePayload {TestInt = 2, TestString = "foo"}, null as Exception),receiveingGroups[1]);
            Assert.Contains((1, "topics/blub3/test", new TestMessagePayload {TestInt = 3, TestString = "bar"}, null as Exception),receiveingGroups[1]);

        }

        
        [Test]
        public void TestMessageReceivingOnMultipleSubscriptionsHavingConversionErrors()
        {
            var client = new ManagedConnectedClient();
            var msgReceiver = new ClientMessageReceiver(client, NullLogger.Instance);

            var receivedMessages = new List<(int, string, TestMessagePayload, Exception)>();

            var subIdA = msgReceiver.Subscribe<TestMessagePayload>(
                "topics/+/test",
                MqttQualityOfServiceLevel.ExactlyOnce,
                (topic, payload, e) =>
                {
                    receivedMessages.Add((0, topic, payload, e));
                    return Task.CompletedTask;
                });

            var subIdB = msgReceiver.Subscribe<TestMessagePayload>(
                "topics/#",
                MqttQualityOfServiceLevel.AtMostOnce,
                (topic, payload, e) =>
                {
                    receivedMessages.Add((1, topic, payload, e));
                    return Task.CompletedTask;
                }
            );


            msgReceiver.HandleApplicationMessageReceivedAsync(CreateMessageObject("topics/blub/test", 1, "blub")).Wait();
            msgReceiver.HandleApplicationMessageReceivedAsync(CreateMessageObject("topics/blub2/tests", 2, "foo", false)).Wait();
            msgReceiver.HandleApplicationMessageReceivedAsync(CreateMessageObject("topics/blub3/test", 3, "bar")).Wait();

            msgReceiver.Unsubscribe(subIdA);
            msgReceiver.Unsubscribe(subIdB);


            Assert.AreEqual(5, receivedMessages.Count);

            var receivingGroups = receivedMessages.GroupBy(x => x.Item1).ToDictionary(x => x.Key, x => x.ToList());
            
            Assert.AreEqual(2, receivingGroups[0].Count());
            Assert.AreEqual(3, receivingGroups[1].Count());
            
            Assert.Contains((0, "topics/blub/test", new TestMessagePayload {TestInt = 1, TestString = "blub"}, null as Exception),receivingGroups[0]);
            Assert.Contains((0, "topics/blub3/test", new TestMessagePayload {TestInt = 3, TestString = "bar"}, null as Exception),receivingGroups[0]);

            Assert.Contains((1, "topics/blub/test", new TestMessagePayload {TestInt = 1, TestString = "blub"}, null as Exception),receivingGroups[1]);
            Assert.Contains((1, "topics/blub3/test", new TestMessagePayload {TestInt = 3, TestString = "bar"}, null as Exception),receivingGroups[1]);

            var receivedWithErrorDuringSerialization = receivingGroups[1].Single(x => x.Item2 == "topics/blub2/tests");
            
            Assert.NotNull(receivedWithErrorDuringSerialization.Item4);
            Assert.Null(receivedWithErrorDuringSerialization.Item3);
            Assert.AreEqual(1, receivedWithErrorDuringSerialization.Item1);

        }

        [Test]
        public void TestSkipMessageOnHandlerThrow()
        {
            var client = new ManagedConnectedClient();
            var msgReceiver = new ClientMessageReceiver(client, NullLogger.Instance);

            var receivedMessages = new List<(string, TestMessagePayload, Exception)>();

            var subId = msgReceiver.Subscribe<TestMessagePayload>(
                "topics/+/test",
                MqttQualityOfServiceLevel.ExactlyOnce,
                (topic, payload, e) => {
                    receivedMessages.Add((topic, payload, e));
                    throw new ArgumentException("This should cause message being skipped");
                });


            msgReceiver.HandleApplicationMessageReceivedAsync(CreateMessageObject("topics/blub/test", 1, "blub")).Wait();

            msgReceiver.Unsubscribe(subId);

            Assert.AreEqual(1, receivedMessages.Count);
        }
        
        private MqttApplicationMessageReceivedEventArgs CreateMessageObject(string topic, int i, string s, bool validObject = true)
        {
            var propertyName = validObject ? "i" : "j";
            
            return new MqttApplicationMessageReceivedEventArgs("sender", new MqttApplicationMessage
            {
                Topic = topic,
                Payload = Encoding.UTF8.GetBytes($"{{ \"{propertyName}\" : \"{i}\",  \"s\" : \"{s}\", }} ")
            },null, null);
        }
    }
}