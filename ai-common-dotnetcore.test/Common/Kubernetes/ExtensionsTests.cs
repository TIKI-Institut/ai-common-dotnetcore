﻿using AiCommon.Common.Kubernetes;
using NUnit.Framework;

namespace AiCommon.Tests.Common.Kubernetes
{
    public class ExtensionsTests
    {
        [Test]
        public void TestNamespace()
        {
            var flavorReference = new FlavorReference
                {Principal = "tiki", Name = "infrastructure", Environment = "dev"};
            Assert.AreEqual("tiki-infrastructure-dev", flavorReference.Namespace());
        }
    }
}