﻿using System.Collections.Generic;
using System.Security.Claims;
using AiCommon.Common.Auth.Context;
using NUnit.Framework;

namespace AiCommon.Tests.Common.Auth.Context
{
    public class DspPrincipalTests
    {
        
        private DspPrincipal CreateTestPrincipal(Claim claim)
        {
            var claims = new List<Claim> {claim};
            var claimsIdentity = new ClaimsIdentity(claims);
            var claimsPrincipal = new ClaimsPrincipal();
            claimsPrincipal.AddIdentity(claimsIdentity);
            var principal = new DspPrincipal(claimsPrincipal);
            return principal;
        }
        
        [Test]
        public void TestPrincipalName()
        {
            var claim = new Claim("iss", "foo/test");
            var principal = CreateTestPrincipal(claim);
            Assert.AreEqual("test", principal.GetPrincipalName());
        }

        [Test]
        public void TestPrincipalNameEmptyIss()
        {
            var claim = new Claim("iss","");
            var principal = CreateTestPrincipal(claim);
            Assert.AreEqual(string.Empty, principal.GetPrincipalName());
        }
        
        [Test]
        public void TestPrincipalNameNullIss()
        {
            var claim = new Claim("foo","");
            var principal = CreateTestPrincipal(claim);
            Assert.AreEqual(null, principal.GetPrincipalName());
        }
    }
}