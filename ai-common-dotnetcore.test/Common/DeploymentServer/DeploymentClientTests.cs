﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using AiCommon.Common.DeploymentServer;
using AiCommon.Common.HttpClient;
using AiCommon.Common.Kubernetes;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using NUnit.Framework;

namespace AiCommon.Tests.Common.DeploymentServer
{
    public class DeploymentClientTests
    {
        [Test]
        public void TestDeploy()
        {
            var deploymentServerConfig = new DeploymentClientConfig {ServerUrl = "http://localhost"};

            var deploymentClient = new DeploymentClient(Options.Create(deploymentServerConfig),
                new TestDefaultAuthWrappingHttpClient(NullLogger<DefaultAuthHttpClient>.Instance),
                NullLogger<DeploymentClient>.Instance);
            var flavorReference = new FlavorReference
            {
                Principal = "tiki", Name = "infrastructure", Environment = "dev", FlavorName = "qos",
                FlavorVersion = "0.0.1"
            };
            var ret = deploymentClient.Deploy<TestResponse>(flavorReference, null);
            Assert.That(ret.Result == (HttpStatusCode.OK, null));
        }
        
        [Test]
        public void TestDeployWithModel()
        {
            var deploymentServerConfig = new DeploymentClientConfig {ServerUrl = "http://localhost"};

            var deploymentClient = new DeploymentClient(Options.Create(deploymentServerConfig),
                new TestHttpClientWithModel(NullLogger<DefaultAuthHttpClient>.Instance),
                NullLogger<DeploymentClient>.Instance);
            var flavorReference = new FlavorReference
            {
                Principal = "tiki", Name = "infrastructure", Environment = "dev", FlavorName = "qos",
                FlavorVersion = "0.0.1"
            };
            var model = new []
            {
                new DeploymentOverride
                {
                    Arguments = new[] {"arg1", "arg2"},
                    NamePostfix = "namePostfix"
                }
            };

            var ret = deploymentClient.Deploy<TestResponse>(flavorReference, model, null);
            Assert.That(ret.Result == (HttpStatusCode.OK, null));
        }

        [Test]
        public void TestErroneousDeploy()
        {
            var deploymentServerConfig = new DeploymentClientConfig {ServerUrl = "http://localhost"};

            var serverClient = new DeploymentClient(Options.Create(deploymentServerConfig),
                new TestDefaultAuthWrappingHttpClient(NullLogger<DefaultAuthHttpClient>.Instance),
                NullLogger<DeploymentClient>.Instance);
            var flavorReference = new FlavorReference
            {
                Principal = "tiki", Name = "infrastructure", Environment = "dev", FlavorName = "qos",
                FlavorVersion = "0.0.2"
            };
            
            var ret = serverClient.Deploy<TestResponse>(flavorReference, null, null);
            Assert.That(ret.Result.Item1 == HttpStatusCode.Unauthorized && ret.Result.Item2.Message == "Authorization has failed");
        }

        [JsonObject]
        private class TestResponse
        {
            public string Message { get; set; }
        }

        private class TestDefaultAuthWrappingHttpClient : DefaultAuthHttpClient
        {
            public TestDefaultAuthWrappingHttpClient(ILogger<DefaultAuthHttpClient> logger) : base(logger)
            {
            }

            public override Task<(HttpStatusCode, TResponseBody)> DoPostWithResponse<TResponseBody>(Uri target, object model, string rawWebToken)
            {
                if ("http://localhost/phase/deploy/tiki/infrastructure/dev/qos/0.0.1" == target.AbsoluteUri)
                {
                    return Task.Run(() => (HttpStatusCode.OK, default(TResponseBody)));
                }
                
                TResponseBody serializedBody;
                var bodyStream = new MemoryStream(Encoding.UTF8.GetBytes("{'message': 'Authorization has failed'}"));
                using (var reader = new StreamReader(bodyStream))
                {
                    var serializer = new JsonSerializer();
                    serializedBody = (TResponseBody) serializer.Deserialize(reader, typeof(TResponseBody));
                }

                return Task.FromResult((HttpStatusCode.Unauthorized, serializedBody));
            }
        }
        
        private class TestHttpClientWithModel : DefaultAuthHttpClient
        {
            public TestHttpClientWithModel(ILogger<DefaultAuthHttpClient> logger) : base(logger)
            {
            }

            public override Task<(HttpStatusCode, TResponseBody)> DoPostWithResponse<TResponseBody>(Uri target, object model, string rawWebToken)
            {
                Assert.AreEqual("http://localhost/phase/deploy/tiki/infrastructure/dev/qos/0.0.1", target.AbsoluteUri);
                var md = (DeploymentOverride[]) model;
                Assert.AreEqual("namePostfix", md[0].NamePostfix);
                return Task.FromResult((HttpStatusCode.OK, default(TResponseBody)));
            }
        }
    }
}