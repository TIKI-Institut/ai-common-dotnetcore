﻿using System;
using System.Collections.Generic;
using System.IO;
using AiCommon.Common.ApplicationStartup.Initializers;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NUnit.Framework;
using Serilog.Core;
using Serilog.Events;
using Serilog.Extensions.Logging;

namespace AiCommon.Tests.Common.ApplicationStartup.Initializers
{
    internal class TestSink : ILogEventSink
    {
        public List<LogEvent> Events { get; } = new List<LogEvent>();

        public void Emit(LogEvent le)
        {
            Events.Add(le);
        }
    }

    public class SerilogTests : SerilogInitializer
    {
        [Test]
        public void TestConfigFileBasedConfigureLogging()
        {
            var loggingConfig = ConfigureLogging();

            var testSink = new TestSink();
            var seriLogger = loggingConfig.WriteTo.Sink(testSink).CreateLogger();
            var logger = new SerilogLoggerProvider(seriLogger).CreateLogger(nameof(SerilogTests));

            logger.LogDebug("1");
            Assert.AreEqual("1", testSink.Events[0].MessageTemplate.Text);
            logger.LogError("2");
            Assert.AreEqual("2", testSink.Events[1].MessageTemplate.Text);
            logger.LogCritical("3");
            Assert.AreEqual("3", testSink.Events[2].MessageTemplate.Text);
            logger.LogInformation("4");
            Assert.AreEqual("4", testSink.Events[3].MessageTemplate.Text);
            logger.LogWarning("5");
            Assert.AreEqual("5", testSink.Events[4].MessageTemplate.Text);

            logger.LogTrace("6");
            Assert.AreEqual(5, testSink.Events.Count);
        }

        [Test]
        public void TestMissingConfigFileConfigureLogging()
        {
            var loggingConfig = ConfigureLogging("nonexistent.dfkjh");

            var testSink = new TestSink();
            var seriLogger = loggingConfig.WriteTo.Sink(testSink).CreateLogger();
            var logger = new SerilogLoggerProvider(seriLogger).CreateLogger(nameof(SerilogTests));

            logger.LogInformation("1");
            Assert.AreEqual("1", testSink.Events[0].MessageTemplate.Text);

            logger.LogDebug("2");
            logger.LogTrace("3");

            Assert.AreEqual(1, testSink.Events.Count);
        }

        [Test]
        public void TestMissingConfigSectionConfigureLogging()
        {
            var loggingConfig = ConfigureLogging("appsettings.missconfigured.json");

            var testSink = new TestSink();
            var seriLogger = loggingConfig.WriteTo.Sink(testSink).CreateLogger();
            var logger = new SerilogLoggerProvider(seriLogger).CreateLogger(nameof(SerilogTests));

            logger.LogInformation("1");
            Assert.AreEqual("1", testSink.Events[0].MessageTemplate.Text);

            logger.LogDebug("2");
            logger.LogTrace("3");

            Assert.AreEqual(1, testSink.Events.Count);
        }

        [Test]
        public void TestConfigureServices()
        {
            var services = new ServiceCollection();
            ConfigureServices(services);
            var sp = services.BuildServiceProvider();

            var ilogger = sp.GetService<ILogger<SerilogTests>>();

            using (var consoleOut = new StringWriter())
            {
                Console.SetOut(consoleOut);

                ilogger.LogInformation("1");
                ilogger.LogDebug("2");

                Assert.IsTrue(consoleOut.ToString().Contains("] - 1"));
                Assert.IsTrue(consoleOut.ToString().Contains("] - 2"));
            }
        }
    }
}