﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using AiCommon.Main.Job;
using CommandLine;

namespace AiCommon.Main
{
    public class EntryPoint
    {

        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        /// # THIS DEPLOYMENT GETS A SUPPLY OF ENV VARS TO WORK WITH
        /// #
        /// # DSP deployment stuff
        /// #  - DSP_WEB_DEPLOYMENT_HOST        the deployment host (i.e. subhost.mainhost.com)
        /// #  - DSP_WEB_DEPLOYMENT_PATH        the relative deployment path the host (i.e. /blub/foo/bar)
        /// #
        /// #  => both, DSP_WEB_DEPLOYMENT_HOST and DSP_WEB_DEPLOYMENT_PATH together make up the reachable URL
        /// #     (i.e. https://subhost.mainhost.com/blub/foo/bar )
        /// #
        /// #
        /// # Main DSP stuff
        /// #  - DSP_PRINCIPAL      the current principal (i.e. tiki)
        /// #  - DSP_NAME           the current deployment name (i.e. ai-examples)
        /// #  - DSP_ENVIRONMENT    the current deployment environment (i.e. python-dev)
        /// #  - DSP_FLAVOR         the current deployment flavor (i.e. webpy36)
        /// #
        /// #
        /// # Security stuff
        /// #  - JWT_ISSUER         the token ISSUER that will be used to validate all provided tokens
        /// #                       (i.e. https://auth.tiki-dsp.io/auth/realms/tiki)
        /// #
        /// #  - OAUTH_CLIENT_ID    the client that will be used to validate all provided tokens
        /// #                       (i.e. ai-examples-python-dev-web-py36)
        /// #
        /// #
        /// </remarks>
        /// <param name="args"></param>
        /// <returns></returns>

        public static int Run(string[] args)
        {
            return Parser.Default
	            .ParseArguments(args, GetSupportedVerbTypes().ToArray())
	            .MapResult(
	                (IJobDeclaration job) => job.Run(),
	                OnCmdParamNotParsed
		        );
        }

		private static IEnumerable<Type> GetSupportedVerbTypes()
	    {
		    return from assembly in AppDomain.CurrentDomain.GetAssemblies()
			    from type in assembly.GetTypes()
			    let typeAttribute = type.GetCustomAttribute<VerbAttribute>()
			    where typeAttribute != null
			    select type;
	    }

	    private static int OnCmdParamNotParsed(IEnumerable<Error> errors)
        {
            foreach (var error in errors.OfType<TokenError>())
            {
                Console.Error.WriteLine($"{error.Tag.ToString()} for token {error.Token}");
            }

            return -1;
        }
    }
}