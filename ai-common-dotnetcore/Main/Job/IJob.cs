﻿using AiCommon.Common.ApplicationStartup;
using AiCommon.Common.ApplicationStartup.Initializers;

namespace AiCommon.Main.Job
{
    public interface IJobBase<TConfig>
    {
        int Run(TConfig config);
    }

    public interface IJob<TConfig, TStartup> : IJobBase<TConfig> where TStartup : IApplicationServiceInitializer
    {
    }

    public interface IJob<TConfig> : IJob<TConfig, SerilogInitializer>
    {
    }
}