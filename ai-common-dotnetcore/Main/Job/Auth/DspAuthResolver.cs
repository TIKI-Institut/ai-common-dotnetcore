﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Threading;
using AiCommon.Common.Auth;
using AiCommon.Common.Auth.Context;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.DependencyInjection;

namespace AiCommon.Main.Job.Auth
{
	public static class DspAuthResolver
	{
		public static void ResolvePrincipal(IServiceProvider serviceProvider, IJwtIssuerContext jwtIssuerContext)
        {
            var claimsTransformer = serviceProvider.GetService<IClaimsTransformation>();
            var oauthContext = serviceProvider.GetService<DspOauthContext>();

            var jwtTokenHandler = new JwtSecurityTokenHandler();
            
            var principal = jwtTokenHandler.ValidateToken(
                GetTokenFromFile(new FileInfo("/dsp/auth/token")), 
                JwtIssuerContext.DefaultTokenValidationParameters(jwtIssuerContext), 
                out var jwtTokenFromSecurityHandler);

            if (jwtTokenFromSecurityHandler is JwtSecurityToken jwtSecurityToken)
            {
                oauthContext.Token = new JwtSecurityTokenSource(jwtSecurityToken);
                oauthContext.Principal = new DspPrincipal(claimsTransformer.TransformAsync(principal).Result);
            }
        }

        private static string GetTokenFromFile(FileInfo targetFile)
        {
            //Wait 60s until you get the token (because sometimes the the oauth sidecar container needs
            //a longer time to retrieve the oauth token)
            for (var i = 0; i < 60 && !targetFile.Exists; i++)
            {
                Thread.Sleep(1000);
            }

            return File.ReadAllText(targetFile.FullName);
        }
    }
}
