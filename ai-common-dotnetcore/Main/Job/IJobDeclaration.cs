﻿namespace AiCommon.Main.Job
{
	public interface IJobDeclaration
	{
        int Run();
    }
}
