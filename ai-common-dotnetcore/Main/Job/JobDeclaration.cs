﻿using System;
using System.Linq;
using System.Reflection;
using System.Security.Authentication;
using AiCommon.Common.ApplicationStartup;
using AiCommon.Common.Auth;
using AiCommon.Common.Auth.Context;
using AiCommon.Main.Job.Auth;
using AiCommon.Main.Roles;
using Microsoft.Extensions.DependencyInjection;
using Samhammer.DependencyInjection;

namespace AiCommon.Main.Job
{
    public abstract class JobDeclaration<TJob, TConfig> : IJobDeclaration where TJob : IJobBase<TConfig>
        where TConfig : JobDeclaration<TJob, TConfig>
    {
        private void ApplyApplicationServiceInitializer(IServiceCollection serviceCollection)
        {
            var exJobInterface = typeof(TJob).GetInterfaces()
                .SingleOrDefault(x => x.IsGenericType && x.GetGenericTypeDefinition() == typeof(IJob<,>));
            
            var startupType = exJobInterface?.GenericTypeArguments[1];

            if (startupType == null) return;
            
            var sProvider = serviceCollection.BuildServiceProvider();
            if (ActivatorUtilities.CreateInstance(sProvider, startupType) is IApplicationServiceInitializer startup)
                startup.ConfigureServices(serviceCollection);
            
        }


        public int Run()
        {
            Console.Out.WriteLine("Starting Job");
            try
            { 
                var serviceCollection = new ServiceCollection();

                var jwtIssuerContext = new EnvironmentalJwtIssuerContext();
                
                //=> basic stuff
                // Register the job and job declaration
                //services.AddSingleton(target.GetType(), target);
                serviceCollection.AddSingleton(typeof(TJob));
                serviceCollection.RegisterAuthContext();
                serviceCollection.AddSingleton<IJwtIssuerContext>(jwtIssuerContext);
                ApplyApplicationServiceInitializer(serviceCollection);
                
                //=> resolving all the dependencies
                serviceCollection.ResolveDependencies(new AllAssembliesLoadingStrategy());

                var serviceProvider = serviceCollection.BuildServiceProvider();

                CheckPermissions(serviceProvider, jwtIssuerContext);

                var jobImpl = ActivatorUtilities.CreateInstance<TJob>(serviceProvider);
                return jobImpl.Run(this as TConfig);
            }
            catch (Exception e)
            {
                Console.Out.WriteLine("Unexpected error while starting job.");
                Console.Out.WriteLine(e.ToString());
                return -1;
            }
        }

        private void CheckPermissions(IServiceProvider serviceProvider, IJwtIssuerContext jwtIssuerContext)
        {
            var roleAttribute = typeof(TJob).GetCustomAttribute<NeedRolesAttribute>();
            if (roleAttribute == null) return;
            
            DspAuthResolver.ResolvePrincipal(serviceProvider, jwtIssuerContext);

            var authContext = serviceProvider.GetService<DspOauthContext>();

            var requiredRoles = typeof(TJob).GetCustomAttribute<NeedRolesAttribute>()?.Role
                .Where(x => !string.IsNullOrEmpty(x)).ToArray();

            var hasPermissionToRun = requiredRoles?.All(r => authContext.Principal.GetRoles(jwtIssuerContext.ClientId).Contains(r)) ?? true;
            if (!hasPermissionToRun)
            {
                throw new AuthenticationException("Required roles not satisfied: " +
                                                  string.Join(", ", requiredRoles));
            }
        }
    }
}