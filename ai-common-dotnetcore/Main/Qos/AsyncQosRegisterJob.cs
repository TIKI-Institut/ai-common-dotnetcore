﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AiCommon.Common.Kubernetes;
using AiCommon.Common.Mqtt;
using AiCommon.Main.Job;
using AiCommon.Main.Qos.Model;
using CommandLine;
using Microsoft.Extensions.Logging;
using MQTTnet.Protocol;

namespace AiCommon.Main.Qos
{
    [Verb("qos-register", HelpText =
        "Registers all async QoS endpoints. Executed during 'provision' step of ai-provisioner")]
    public class AsyncQosRegisterOptions : JobDeclaration<AsyncQosRegisterJob, AsyncQosRegisterOptions>
    {
    }

    public class AsyncQosRegisterJob : IJob<AsyncQosRegisterOptions, AsyncQosJobInitializer>
    {
        private readonly IEnumerable<QosEntry> _asyncQosEntries;
        private readonly ICurrentFlavorReference _flavorReference;
        private readonly ILocalNamespaceBrokerClientProvider _localMqttClientProvider;
        private readonly ILogger<AsyncQosRegisterJob> _logger;

        /// <summary>
        ///     Is invoked during flavor registration (in DSP during 'provision' step of ai-provisioner by Deployment Server
        ///     as a separate deployment) which collects all AsyncQosEntries defined within flavor
        ///     and publishes them onto mqtt topic Topics.NamespaceBrokerFlavorAsyncQosRegisterTopic.
        ///     They can be then collected from mqtt-topic and processed (in DSP collected by qos-manager and stored).
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="asyncQosEntries">are injected by DI as collection of all IAsyncQosEntry's annotated for injection</param>
        /// <param name="localMqttClientProvider"></param>
        /// <param name="currentFlavorReference"></param>
        public AsyncQosRegisterJob(
            ILogger<AsyncQosRegisterJob> logger,
            IEnumerable<QosEntry> asyncQosEntries,
            ILocalNamespaceBrokerClientProvider localMqttClientProvider,
            // Do NOT generalize interface since it has to be like this for DI
            // ReSharper disable once SuggestBaseTypeForParameter
            ICurrentFlavorReference currentFlavorReference)
        {
            _logger = logger;
            _flavorReference = currentFlavorReference;
            _logger = logger;
            _asyncQosEntries = asyncQosEntries.Where(x => x.Type == QosType.Async);
            _localMqttClientProvider = localMqttClientProvider;
        }

        public int Run(AsyncQosRegisterOptions registerOptions)
        {
            _logger.LogInformation($"Registration of async QoS entries in flavor: {_flavorReference}...");
            try
            {
                RegisterAsyncQosEntries().Wait();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Failed to register QOS entries");
                return 1;
            }

            _logger.LogInformation($"Registration of async QoS entries in flavor: {_flavorReference}... finished with {_asyncQosEntries?.Count()} entries.");
            return 0;
        }

        private async Task RegisterAsyncQosEntries()
        {
            if (_asyncQosEntries.Any())
            {
                var flavor = FlavorReference.FromFlavorReference(_flavorReference);
                var entries = _asyncQosEntries.ToList();

                var client = _localMqttClientProvider.CreateClient("async-qos-register");
                try
                {
                    await client.Connect();

                    _logger.LogDebug($"Publishing {entries.Count()} registered async QOS entries of flavor: " +
                                     $"{flavor} to topic: {Topics.NamespaceBrokerFlavorAsyncQosRegisterTopic}...");
                    await client.Publish(Topics.NamespaceBrokerFlavorAsyncQosRegisterTopic, new QosEndpoint
                    {
                        FlavorReference = flavor,
                        Entries = new List<QosEntry>(entries)
                    }, MqttQualityOfServiceLevel.AtLeastOnce);
                }
                finally
                {
                    _logger.LogDebug($"Publishing {entries.Count()} registered async QOS entries of flavor: " +
                                     $"{flavor} to topic: {Topics.NamespaceBrokerFlavorAsyncQosRegisterTopic}... finished. Disconnecting...");
                    await client.Disconnect();
                }
            }
            else
            {
                _logger.LogDebug("No (async) QOS endpoints were found");
            }
        }
    }
}