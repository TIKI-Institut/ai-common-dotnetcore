﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using AiCommon.Main.Qos.Model;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Samhammer.DependencyInjection.Attributes;
using Samhammer.DependencyInjection.Providers;
using Samhammer.DependencyInjection.Utils;

namespace AiCommon.Main.Qos
{
    public class QosEntryInjectionProvider : IServiceDescriptorProvider
    {
        private readonly ILogger<QosEntryInjectionProvider> _logger;
        private readonly IAssemblyResolvingStrategy _assemblyResolvingStrategy;

        public QosEntryInjectionProvider(ILogger<QosEntryInjectionProvider> logger,
            IAssemblyResolvingStrategy assemblyResolvingStrategy)
        {
            _logger = logger;
            _assemblyResolvingStrategy = assemblyResolvingStrategy;
        }

        public IEnumerable<ServiceDescriptor> ResolveServices()
        {
            var list = _assemblyResolvingStrategy.ResolveAssemblies().ToList();
            _logger.LogTrace("Loaded assemblies: {Assemblies}.",
                (object) list.Select(a => a.GetName().Name));

            foreach (var serviceDescriptor in ServiceDescriptors(list, typeof(QosEntry)))
                yield return serviceDescriptor;
        }

        private IEnumerable<ServiceDescriptor> ServiceDescriptors(IEnumerable<Assembly> list, Type parentType)
        {
            var typesWithParent =
                ReflectionUtils.FindAllExportedTypesWithParentType(list, parentType);
            _logger.LogTrace("Loaded types with attribute {Attribute}: {Types}.",
                (object) typeof(DependencyInjectionAttribute),
                (object) typesWithParent);
            foreach (Type type in typesWithParent)
            {
                yield return new ServiceDescriptor(parentType, type, ServiceLifetime.Scoped);
            }
        }
    }
}