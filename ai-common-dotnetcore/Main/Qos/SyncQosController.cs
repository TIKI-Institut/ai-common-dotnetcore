using System;
using System.Collections.Generic;
using System.Linq;
using AiCommon.Common.Kubernetes;
using AiCommon.Main.Qos.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AiCommon.Main.Qos
{
    public class SyncQosController : ControllerBase
    {
        private readonly ILogger<SyncQosController> _logger;
        private readonly ICurrentFlavorReference _currentFlavor;
        private readonly ICurrentPodReference _currentPod;
        private readonly IEnumerable<QosEntry> _syncQosEntries;

        /// <summary>
        /// Used by WebKernels to provide additional web routes for QoS operations within flavor. Currently these
        /// operations are supported
        /// - list - lists all the declared synchronous QoS entries
        /// - run - executes synchronous QoS run and returns QoS result
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="syncQosEntries">are injected by DI as collection of all ISyncQosEntry's annotated for injection</param>
        /// <param name="currentFlavor"></param>
        /// <param name="currentPod"></param>
        public SyncQosController(ILogger<SyncQosController> logger, IEnumerable<QosEntry> syncQosEntries, 
            ICurrentFlavorReference currentFlavor, ICurrentPodReference currentPod) {
            _logger = logger;
            _syncQosEntries = syncQosEntries.Where(x => x.Type == QosType.Sync);
            _currentFlavor = currentFlavor;
            _currentPod = currentPod;
        }
        
        [HttpGet]
        public IActionResult List()
        {
            var podReference = PodReference.FromPodReference(_currentPod);
            var flavorReference = FlavorReference.FromFlavorReference(_currentFlavor);

            _logger.LogDebug($"About to list sync QoS entries for flavor: {flavorReference} and pod: {podReference}");
            var now = DateTime.Now;
            var endpoint = new QosEndpoint
            {
                PodReference = podReference,
                FlavorReference = flavorReference,
                InitialBeat = now,
                LastBeat = now,
                Entries = new List<QosEntry>(_syncQosEntries.ToList())
            };
            
            _logger.LogInformation($"Listing [{endpoint.Entries.Count()}] registered sync QoS entries for flavor: {flavorReference} and pod: {podReference}");
            return new OkObjectResult(endpoint);
        }

        
        [HttpGet]
        public IActionResult Run(string transactionId, string name)
        {
            _logger.LogDebug($"Starting sync QoS run for [id:{transactionId}] '{name}'");
            var qosResult = new QosRunResult
            {
                Id = int.Parse(transactionId),
                Status = QosRunStatus.NotFound,
                StatusMessage = $"Sync QoS entry '{name}' was not found",
                PodReference = PodReference.FromPodReference(_currentPod),
                FlavorReference = FlavorReference.FromFlavorReference(_currentFlavor)
            };

            var syncQosEntry = _syncQosEntries.FirstOrDefault(x => x.Name == name);
            if (syncQosEntry != null)
            {
                qosResult.QosEntry = syncQosEntry;

                _logger.LogDebug($"Running sync QoS entry [id:{transactionId}] '{name}'");
                try
                {
                    qosResult.Score = syncQosEntry.Run();
                    qosResult.Status = QosRunStatus.Success;
                    qosResult.ProducedOn = DateTime.Now;
                    qosResult.StatusMessage = $"Sync QoS run for {name} completed";
                }
                catch(Exception e)
                {
                    var msg = $"Running sync QoS for [id:{transactionId}] '{name}' has failed"; 
                    _logger.LogError(e, msg);

                    qosResult.Status = QosRunStatus.Failed;
                    qosResult.StatusMessage = new Exception(msg, e).ToString();
                }
            } 
            else
            {
                _logger.LogWarning($"Sync QoS entry [{transactionId}] '{name}' not found");
            }

            return new OkObjectResult(qosResult);
        }
    }
}
