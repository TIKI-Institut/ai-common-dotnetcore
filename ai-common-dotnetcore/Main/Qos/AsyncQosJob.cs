﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AiCommon.Common.Kubernetes;
using AiCommon.Common.Mqtt;
using AiCommon.Main.Job;
using AiCommon.Main.Qos.Model;
using CommandLine;
using Microsoft.Extensions.Logging;
using MQTTnet.Protocol;

namespace AiCommon.Main.Qos
{
    [Verb("qos-run", HelpText = "Runs a previously declared async QoS endpoint.")]
    public class AsyncQosJobOptions : JobDeclaration<AsyncQosJob, AsyncQosJobOptions>
    {
        [Value(0, Required = true, MetaName = "async QoS entry id that triggered this run")]
        // ReSharper disable once UnusedAutoPropertyAccessor.Global
        public string TransactionId { get; set; }

        [Value(1, Required = true, MetaName = "name of the async QoS job to be run")]
        public string Name { get; set; }
    }

    public class AsyncQosJob : IJob<AsyncQosJobOptions, AsyncQosJobInitializer>
    {
        private readonly IEnumerable<QosEntry> _asyncQosEntries;
        private readonly ICurrentFlavorReference _currentFlavor;
        private readonly ILocalNamespaceBrokerClientProvider _localMqttClientProvider;
        private readonly ILogger<AsyncQosJob> _logger;

        public AsyncQosJob(ILogger<AsyncQosJob> logger, IEnumerable<QosEntry> asyncQosEntries,
            ILocalNamespaceBrokerClientProvider localMqttClientProvider, ICurrentFlavorReference currentFlavor)
        {
            _logger = logger;
            _asyncQosEntries = asyncQosEntries;
            _localMqttClientProvider = localMqttClientProvider;
            _currentFlavor = currentFlavor;
        }

        public int Run(AsyncQosJobOptions listOptions)
        {
            _logger.LogInformation($"Starting async QoS run for [id: {listOptions.TransactionId}] '{listOptions.Name}'...");
            var qosResult = new QosRunResult
            {
                Id =  int.Parse(listOptions.TransactionId),
                Status = QosRunStatus.NotFound,
                StatusMessage = $"Async QoS entry '{listOptions.Name}' was not found",
                FlavorReference = FlavorReference.FromFlavorReference(_currentFlavor)
            };

            var target = _asyncQosEntries.FirstOrDefault(x => x.Name == listOptions.Name);
            if (target != null)
            {
                qosResult.QosEntry = target;

                _logger.LogDebug($"Running async QoS for [id:{listOptions.TransactionId}] '{listOptions.Name}'");
                try
                {
                    qosResult.Score = target.Run();
                    qosResult.ProducedOn = DateTime.Now;
                    qosResult.Status = QosRunStatus.Success;
                    qosResult.StatusMessage = $"Async QoS run for {listOptions.Name} completed";
                }
                catch (Exception e)
                {
                    var msg = $"Running async QoS for [id:{listOptions.TransactionId}] '{listOptions.Name}' has failed";
                    _logger.LogError(e, msg);

                    qosResult.Status = QosRunStatus.Failed;
                    qosResult.StatusMessage = new Exception(msg, e).ToString();
                }
            }
            else
            {
                _logger.LogWarning($"Async QoS entry [id:{listOptions.TransactionId}] '{listOptions.Name}' was not found");
            }

            _logger.LogDebug($"Publishing async QoS result for [id:{listOptions.TransactionId}] '{listOptions.Name}'");
            try
            {
                PublishResults(qosResult).Wait();
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"Publishing async QoS result for entry [{listOptions.TransactionId}] '{listOptions.Name}' has failed");
                return 1;
            }

            _logger.LogInformation($"Async QoS run for entry [{listOptions.TransactionId}] '{listOptions.Name}' has completed successfully");
            return 0;
        }

        private async Task PublishResults(QosRunResult reportingResult)
        {
            var client = _localMqttClientProvider.CreateClient("async-qos-register");

            await client.Connect();

            _logger.LogDebug(
                $"Publishing QoS run results for entry '{reportingResult.FlavorReference}' to topic '{Topics.NamespaceBrokerFlavorAsyncQosResultTopic}'");
            await client.Publish(Topics.NamespaceBrokerFlavorAsyncQosResultTopic, reportingResult,
                MqttQualityOfServiceLevel.AtLeastOnce);

            await client.Disconnect();
        }
    }
}