﻿using AiCommon.Common.ApplicationStartup;
using AiCommon.Common.ApplicationStartup.Initializers;
using Microsoft.Extensions.DependencyInjection;
using Samhammer.DependencyInjection.Providers;

namespace AiCommon.Main.Qos
{
    public class AsyncQosJobInitializer : IApplicationServiceInitializer
    {
        public void ConfigureServices(IServiceCollection serviceCollection)
        {
            new SerilogInitializer().ConfigureServices(serviceCollection);
            serviceCollection.AddSingleton<IServiceDescriptorProvider, QosEntryInjectionProvider>();
        }
    }
}