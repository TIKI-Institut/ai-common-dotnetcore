using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace AiCommon.Main.Qos.Model
{
    public enum QosType 
    {
        Async,
        Sync,
        Dynamic
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class QosEntry 
    {
        [JsonProperty("id")] 
        public int Id { get; set; }

        [JsonProperty("name", Required = Required.Always)]
        // ReSharper disable once UnassignedGetOnlyAutoProperty
        public virtual string Name { get; set; }

        [JsonProperty("type", Required = Required.Always)]
        [JsonConverter(typeof(StringEnumConverter))]
        public virtual QosType Type { get; set;}
        
        [JsonProperty("active")]
        public virtual Boolean Active { get; set; }
        
        [JsonProperty("qosEndpoint")]
        public QosEndpoint QosEndpoint{ get; set; }

        public virtual double Run()
        {
            throw new NotImplementedException("The 'Run'' method of QosEntry specialization has to be overridden");
        }

        public override string ToString() => $"{Type} QoS Entry '{Name}'";
    }
}