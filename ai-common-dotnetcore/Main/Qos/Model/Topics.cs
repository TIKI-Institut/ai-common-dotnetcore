namespace AiCommon.Main.Qos.Model
{
    public static class Topics
    {
        public const string NamespaceBrokerFlavorAsyncQosResultTopic = "qos/result/async";
        public const string NamespaceBrokerFlavorSyncQosResultTopic = "qos/result/sync";
        public const string NamespaceBrokerFlavorDynamicQosResultTopic = "qos/result/dynamic";

        private const string NamespaceBrokerFlavorQosEntryRegisterTopic = "qos/register/";

        public static readonly string NamespaceBrokerFlavorSyncQosRegisterTopic =
            $"{NamespaceBrokerFlavorQosEntryRegisterTopic}sync";

        public static readonly string NamespaceBrokerFlavorAsyncQosRegisterTopic =
            $"{NamespaceBrokerFlavorQosEntryRegisterTopic}async";
        
        #region QoS-Manager from Frontend Topics

        public static string QosManagerAsyncQosTriggerTopic => "qos/trigger/async";
        public static string QosManagerSyncQosTriggerTopic => "qos/trigger/sync";

        #endregion
    }
}