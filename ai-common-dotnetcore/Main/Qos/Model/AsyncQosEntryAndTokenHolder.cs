﻿using Newtonsoft.Json;

namespace AiCommon.Main.Qos.Model
{
    [JsonObject(MemberSerialization.OptIn)]
    public class AsyncQosEntryAndTokenHolder
    {
        [JsonProperty("asyncQosEntry")]
        public QosEntry AsyncQosEntry { get; set; }
        
        [JsonProperty("token")]
        public string WebToken { get; set; }
    }
}