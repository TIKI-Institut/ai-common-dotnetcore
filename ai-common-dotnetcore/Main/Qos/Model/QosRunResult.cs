using System;
using AiCommon.Common.Kubernetes;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace AiCommon.Main.Qos.Model
{
    public enum QosRunStatus
    {
        Success,
        Failed,
        NotFound,
        Running
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class QosRunResult
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("producedOn", Required = Required.Always)]
        public DateTime ProducedOn { get; set; }

        [JsonProperty("status", Required = Required.Always)]
        [JsonConverter(typeof(StringEnumConverter))]
        public QosRunStatus Status { get; set; }

        [JsonProperty("statusMessage", Required = Required.AllowNull)]
        public string StatusMessage { get; set; }
        
        [JsonProperty("parameters")]
        public string Parameters { get; set; }

        [JsonProperty("score")] public double? Score { get; set; }
        
        [JsonProperty("flavorReference", Required = Required.Always)]
        public FlavorReference FlavorReference { get; set; }
        
        [JsonProperty("podReference")]
        public PodReference PodReference { get; set; }
        
        [JsonProperty("qosEntry", Required = Required.Always)]
        public QosEntry QosEntry { get; set; }

        public override string ToString() =>
            $"id:{Id}, producedOn:{ProducedOn}, status:{Status}, message:{StatusMessage}" +
            $", score:{Score}, flavor:{FlavorReference}";
    }
}