using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using AiCommon.Common.Kubernetes;
using Newtonsoft.Json;

namespace AiCommon.Main.Qos.Model
{
    [JsonObject(MemberSerialization.OptIn)]
    public class QosEndpoint
    {
        public QosEndpoint()
        {
        }

        public QosEndpoint(IFlavorReference flavorReference, IPodReference pod,
            DateTime initialBeat, DateTime lastBeat, bool isLost = false, int countBeatsSinceInitial = 0)
        {
            FlavorReference = FlavorReference.FromFlavorReference(flavorReference);
            PodReference = PodReference.FromPodReference(pod);
            InitialBeat = initialBeat;
            LastBeat = lastBeat;
            IsLost = isLost;
            CountBeatsSinceInitial = countBeatsSinceInitial;
        }

        public QosEndpoint(IFlavorReference flavorReference, IPodReference pod,
            DateTime initialBeat, bool isLost = false, int countBeatsSinceInitial = 0) :
            this(flavorReference, pod, initialBeat, initialBeat, isLost, countBeatsSinceInitial)
        {
        }
        
        [JsonProperty("id")] 
        public int Id { get; set; }

        [JsonProperty("entries")] public List<QosEntry> Entries { get; set; }        

        [JsonProperty("flavorReference")] public FlavorReference FlavorReference { get; set; }
        
        [JsonProperty("podReference")] public PodReference PodReference { get; set; }

        [JsonProperty("initialBeat")] public DateTime InitialBeat { get; set; }

        [JsonProperty("lastBeat")] public DateTime LastBeat { get; set; }

        [JsonProperty("beatsSinceInitial")] public int CountBeatsSinceInitial { get; set; }

        [JsonProperty("isLost")] public bool IsLost { get; set; }

        public override string ToString() => PodReference == null ? "unset" : $"sync QoS Endpoint: {PodReference}";


        protected bool Equals(QosEndpoint other)
        {
            return Equals(FlavorReference, other.FlavorReference) && Equals(PodReference, other.PodReference);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == this.GetType() && Equals((QosEndpoint) obj);
        }

        [SuppressMessage("ReSharper", "NonReadonlyMemberInGetHashCode")]
        public override int GetHashCode()
        {
            unchecked
            {
                return ((FlavorReference != null ? FlavorReference.GetHashCode() : 0) * 397) ^ (PodReference != null ? PodReference.GetHashCode() : 0);
            }
        }

        public static bool operator ==(QosEndpoint left, QosEndpoint right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(QosEndpoint left, QosEndpoint right)
        {
            return !Equals(left, right);
        }
    }
}