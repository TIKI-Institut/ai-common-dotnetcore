using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using AiCommon.Common.ApplicationStartup;
using AiCommon.Common.ApplicationStartup.Initializers;
using AiCommon.Main.Job;
using CommandLine;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Serilog;
using Microsoft.Extensions.Hosting;


namespace AiCommon.Main.Web
{
    [Verb(MainAppSelector, HelpText = "Runs an web server with all declared endpoints.")]
    public class WebMain : IJobDeclaration
    {
        public const string MainAppSelector = "run-web";

        [Value(0)] public IEnumerable<string> Arguments { get; set; }


        private static IHost BuildWebHost(string[] args)
        {
            var hostBuilder = Host
                .CreateDefaultBuilder(args)
                .UseApplicationServiceInitializer<SerilogInitializer>()
                .ConfigureAppConfiguration(SetAppSettingsConfig)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                    webBuilder.UseUrls("http://0.0.0.0:5000");
                });

            return hostBuilder.Build();
        }

        private static void SetAppSettingsConfig(HostBuilderContext builderContext, IConfigurationBuilder config)

        {
            var env = builderContext.HostingEnvironment;

            Log.Information($"Loading configuration in Environment '{env.EnvironmentName}'");

            config.SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", true, true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true);
        }

        public int Run()
        {
            try
            {
                Log.Information("Starting web application.");
                BuildWebHost(Arguments?.ToArray()).Run();
            }
            catch (Exception e)
            {
                Log.Error(e, "Unexpected error while starting web application.");
                return -1;
            }

            return 0;
        }
    }
}