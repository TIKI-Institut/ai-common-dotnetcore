using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;
using AiCommon.Common.Auth;
using AiCommon.Common.Auth.Context;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace AiCommon.Main.Web.Auth
{
    public class DefaultBearerOptionsProvider : IJwtBearerOptionsProvider
    {
        private readonly ILogger<DefaultBearerOptionsProvider> _logger;

        public DefaultBearerOptionsProvider(ILogger<DefaultBearerOptionsProvider> logger)
        {
            _logger = logger;
        }

        protected JwtBearerEvents JwtBearerEvents => new JwtBearerEvents
            {OnTokenValidated = SetTokenToContext, OnAuthenticationFailed = OnAuthenticationFailed};

        protected Task SetTokenToContext(TokenValidatedContext context)
        {
            if (context.SecurityToken is JwtSecurityToken jwtSecurityToken)
            {
                _logger.LogDebug($"Authentication succeeded for token id ${jwtSecurityToken.Id}");

                var claimsTransformer = context.HttpContext.RequestServices.GetService<IClaimsTransformation>();
                var oauthContext = context.HttpContext.RequestServices.GetService<DspOauthContext>();

                oauthContext.Token = new JwtSecurityTokenSource(jwtSecurityToken);
                oauthContext.Principal = new DspPrincipal(claimsTransformer.TransformAsync(context.Principal).Result);
            }

            return Task.CompletedTask;
        }

        private Task OnAuthenticationFailed(AuthenticationFailedContext context)
        {
            // In case of an auth failure the DisableRequestSizeLimit attribute is not read yet.
            // This would lead to an error 500 ("BadHttpRequestException: Request body too large")
            // on uploading large files although the entire request body is not read anyway.
            DisableRequestBodySizeValidationOnce(context);

            _logger.LogDebug($"Authentication failed. Reason: ${context.Exception}");

            // Send authentication failed response
            context.Response.StatusCode = 401;
            return Task.FromResult(0);
        }

        private static void DisableRequestBodySizeValidationOnce(AuthenticationFailedContext context)
        {
            var maxRequestBodySizeFeature = context.HttpContext.Features.Get<IHttpMaxRequestBodySizeFeature>();
            if (maxRequestBodySizeFeature != null)
            {
                maxRequestBodySizeFeature.MaxRequestBodySize = null;
            }
        }

        public virtual void ConfigureServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddSingleton<IJwtIssuerContext, EnvironmentalJwtIssuerContext>();
        }

        public virtual void Configure(JwtBearerOptions o)
        {
            var jwtIssuerContext = new EnvironmentalJwtIssuerContext();

            if (!jwtIssuerContext.IsConfigured)
            {
                _logger.LogWarning("Issuer or oauth client id is not defined! The JwtBearerOptions will not be set");
                return;
            }

            o.Authority = jwtIssuerContext.Issuer;
            o.Audience = jwtIssuerContext.ClientId;
            o.TokenValidationParameters = JwtIssuerContext.DefaultTokenValidationParameters(jwtIssuerContext);
            o.Events = JwtBearerEvents;
        }
    }
}