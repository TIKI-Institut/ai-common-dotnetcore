﻿using System;
using System.Linq;
using AiCommon.Common.Auth;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace AiCommon.Main.Web.Auth
{

	public interface IJwtBearerOptionsProvider
	{
		void ConfigureServices(IServiceCollection serviceCollection);

		void Configure(JwtBearerOptions options);
	}

	public static class WebAuthExtensions
	{
		public static IServiceCollection ConfigureJwtAuthentication(this IServiceCollection serviceCollection)
		{

			//Fallback to default impl
			serviceCollection.TryAdd(new ServiceDescriptor(typeof(IJwtBearerOptionsProvider), typeof(DefaultBearerOptionsProvider), ServiceLifetime.Singleton));
			
			var authBuilder = serviceCollection.AddAuthentication(options =>
			{
				options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
				options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
			});

			var sp = serviceCollection.BuildServiceProvider();

			var authSetupSupport = sp.GetService<IJwtBearerOptionsProvider>();
			
			authSetupSupport.ConfigureServices(serviceCollection);
			
			authBuilder.AddJwtBearer(authSetupSupport.Configure);

			if (serviceCollection.All(x => x.ServiceType != typeof(IJwtIssuerContext)))
			{
				throw new NotSupportedException($"It seems you customized JwtBearerOptions but did not installed a handler for type [{typeof(IJwtIssuerContext)}] which is mandatory.");
			}
			
			return serviceCollection;
		}
	}
}
