﻿using System;
using AiCommon.Main.Qos;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApplicationModels;

namespace AiCommon.Main.Web.Routing
{
	public class RouteConvention : IControllerModelConvention
	{
		private const string ControllerPlaceholder = "[controller]";
		private const string QosControllerEndpoint = "/qos/[action]";

		private AttributeRouteModel DefaultAttributeRouteModel(SelectorModel model)
		{
			var dspDeploymentPath = Environment.GetEnvironmentVariable("DSP_WEB_DEPLOYMENT_PATH");

			if (string.IsNullOrEmpty(dspDeploymentPath) )
			{
				throw new NotSupportedException();
			}

			var subRoute = model?.AttributeRouteModel?.Template ?? ControllerPlaceholder;
			var baseRoute = dspDeploymentPath.TrimEnd('/');
			
			var routeTemplate = $"{baseRoute}/{subRoute}";

			return new AttributeRouteModel(new RouteAttribute(routeTemplate));
		}
		
		private AttributeRouteModel QosAttributeRouteModel(SelectorModel model)
		{
			return new AttributeRouteModel(new RouteAttribute(QosControllerEndpoint));
		}

		
		public void Apply(ControllerModel controller)
		{
			foreach (var selectorModel in controller.Selectors)
			{
				Func<SelectorModel, AttributeRouteModel> routeModelFnc = DefaultAttributeRouteModel;
					
				if (controller.ControllerType == typeof(SyncQosController))
				{
					routeModelFnc = QosAttributeRouteModel;
				}

				selectorModel.AttributeRouteModel = routeModelFnc(selectorModel);
			}
		}
	}
}
