﻿using System.Reflection;
using AiCommon.Common.ApplicationStartup;
using AiCommon.Common.ApplicationStartup.Initializers;
using AiCommon.Common.Auth;
using AiCommon.Main.Qos;
using AiCommon.Main.Web.Auth;
using AiCommon.Main.Web.Routing;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Samhammer.DependencyInjection;
using Samhammer.DependencyInjection.Providers;

namespace AiCommon.Main.Web
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            var entryAssembly = Assembly.GetEntryAssembly();

            services
                .UseApplicationServiceInitializer<SerilogInitializer>()
                .AddSingleton<IServiceDescriptorProvider, QosEntryInjectionProvider>()
                .ResolveDependencies(new AllAssembliesLoadingStrategy())
                .UseModularApplicationInitialization(WebMain.MainAppSelector)
                .AddMvcCore(opts => opts.Conventions.Add(new RouteConvention()))
                .AddApplicationPart(entryAssembly)
                .AddApplicationPart(typeof(Startup).Assembly)
                .AddFormatterMappings()
#if NETCOREAPP3_1
				.AddNewtonsoftJson()
#elif NETCOREAPP2_2
                .AddJsonFormatters()
#endif
                .AddAuthorization()
                .AddCors();

            services
                .UseApplicationServiceInitializer<SerilogInitializer>()
                .RegisterAuthContext()
                .ConfigureJwtAuthentication();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder applicationBuilder)
        {
#if NETCOREAPP3_1
            applicationBuilder.UseRouting();
            applicationBuilder.UseCors();

            applicationBuilder.UseAuthentication();
            applicationBuilder.UseAuthorization();

            applicationBuilder.UseEndpoints(endpoints => { endpoints.MapControllers(); });
#elif NETCOREAPP2_2
            applicationBuilder

                .UseAuthentication()

                .UseMvc();
#endif
        }
    }
}