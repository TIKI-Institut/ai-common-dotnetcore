﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using AiCommon.Common.ApplicationStartup.Initializers;
using AiCommon.Main.Job;
using CommandLine;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Newtonsoft.Json;
using Serilog;

namespace AiCommon.Main.Roles
{
    [AttributeUsage(AttributeTargets.Class)]
    public class NeedRolesAttribute : Attribute
    {
        public IEnumerable<string> Role { get; }

        public NeedRolesAttribute(params string[] role)
        {
            Role = role;
        }
    }

    [JsonObject(MemberSerialization.OptIn)]
    class RolesRepresentation
    {
        [JsonProperty("roles")] public IEnumerable<string> Roles { get; set; }
    }
    

    [Verb("roles", HelpText = "Collects all roles for propagation to KeyCloak.")]
    public class RolesOptions : JobDeclaration<RolesMain, RolesOptions>
    {
        [Option("file")]
        public FileInfo RolesFile { get; set; }
    }

    public class RolesMain : IJob<RolesOptions,SerilogInitializer>
    {
	    private readonly ILogger<RolesMain> _logger;

	    public RolesMain(ILogger<RolesMain> logger)
	    {
		    _logger = logger;
	    }


	    public int Run(RolesOptions options)
		{
			_logger.LogInformation("Resolving roles");
			try
			{
				if (options.RolesFile != null)
				{
					//check parent dir for existence (and create it if not...)
					if (!options.RolesFile?.Directory?.Exists ?? false)
                        options.RolesFile.Directory.Create();

					using (var writer = options.RolesFile.CreateText())
					{
						writer.Write(JsonConvert.SerializeObject(ListRoles()));
					}
				}
				else
				{
					Console.Out.WriteLine(JsonConvert.SerializeObject(ListRoles()));
				}
			}
			catch (Exception e)
			{
				_logger.LogError(e, "An error occured while collecting roles");
				return -1;
			}

			return 0;
		}

		private static RolesRepresentation ListRoles()
		{
			var roles = GetAllRoles();
			
			return new RolesRepresentation { Roles = roles };
		}

        private static IEnumerable<string> ResolveWebRoles(AuthorizeAttribute authorizeAttribute)
        {
            return authorizeAttribute.Roles?.Split(",") ?? new string[0];
        }

        private static IEnumerable<string> ResolveJobRoles(NeedRolesAttribute authorizeAttribute)
        {
            return authorizeAttribute.Role;
        }

        private static IEnumerable<string> ResolveRoles(ICustomAttributeProvider authorizeAttribute)
        {
            var webAttr = from a in authorizeAttribute.GetCustomAttributes(typeof(AuthorizeAttribute), true)
                from r in ResolveWebRoles((AuthorizeAttribute)a)
                select r;

            var jobAttr = from a in authorizeAttribute.GetCustomAttributes(typeof(NeedRolesAttribute), true)
                from r in ResolveJobRoles((NeedRolesAttribute)a)
                select r;

            return jobAttr.Concat(webAttr);
        }


        private static IEnumerable<string> GetAllRoles()
		{
            var types =
                (from assembly in AppDomain.CurrentDomain.GetAssemblies()
                from type in assembly.GetTypes()
                select type).ToArray();

            return types.SelectMany(ResolveRoles)
                .Concat(types.SelectMany(t => t.GetMethods()).SelectMany(ResolveRoles))
                .Select (a => a.Trim())
				.Distinct();
		}
	}
}