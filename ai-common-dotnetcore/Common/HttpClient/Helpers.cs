﻿using System.Net;

namespace AiCommon.Common.HttpClient
{
    public static class Helpers
    {
        public static bool IsSuccessStatusCode(this HttpStatusCode s)
        {
            if (s >= HttpStatusCode.OK)
                return s <= (HttpStatusCode) 299;
            return false;
        }
    }
}