﻿using System;
using System.Net;
using System.Threading.Tasks;

namespace AiCommon.Common.HttpClient
{
    public interface IHttpClient
    {
        Task<HttpStatusCode> DoPost(Uri target, object model);

        Task<(HttpStatusCode, TResponseBody)> DoPostWithResponse<TResponseBody>(Uri target, object model)
            where TResponseBody : new();

        Task<HttpStatusCode> DoGet(Uri target);

        Task<(HttpStatusCode, TResponseBody)> DoGetWithResponse<TResponseBody>(Uri target) where TResponseBody : new();

        Task<HttpStatusCode> DoPut(Uri target, object model);

        Task<(HttpStatusCode, TResponseBody)> DoPutWithResponse<TResponseBody>(Uri target, object model)
            where TResponseBody : new();
    }

    public interface IAuthHttpClient : IHttpClient
    {
        Task<HttpStatusCode> DoPost(Uri target, object model, string rawWebToken);

        Task<(HttpStatusCode, TResponseBody)> DoPostWithResponse<TResponseBody>(Uri target, object model, string rawWebToken)
            where TResponseBody : new();

        Task<HttpStatusCode> DoGet(Uri target, string rawWebToken);

        Task<(HttpStatusCode, TResponseBody)> DoGetWithResponse<TResponseBody>(Uri target, string rawWebToken) where TResponseBody : new();

        Task<HttpStatusCode> DoPut(Uri target, object model, string rawWebToken);

        Task<(HttpStatusCode, TResponseBody)> DoPutWithResponse<TResponseBody>(Uri target, object model, string rawWebToken)
            where TResponseBody : new();
    }
}