﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Samhammer.DependencyInjection.Attributes;

namespace AiCommon.Common.HttpClient
{
    [InjectAs(typeof(IHttpClient), ServiceLifetime.Transient)]
    public class DefaultHttpClient : IHttpClient
    {
        private readonly ILogger<DefaultHttpClient> _logger;
        
        protected const string DeploymentRequestContentType = "application/json";

        protected virtual System.Net.Http.HttpClient HttpClient => new System.Net.Http.HttpClient();

        public DefaultHttpClient(ILogger<DefaultHttpClient> logger)
        {
            _logger = logger;
        }

        private async Task<HttpResponseMessage> DoInternal(Uri target, HttpMethod method, object model)
        {
            var request = new HttpRequestMessage
            {
                RequestUri = target,
                Method = method,
            };

            return await DoInternal(request, model);
        }
        
        protected async Task<HttpResponseMessage> DoInternal(HttpRequestMessage request, object model)
        {
            if (model != null)
                request.Content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, DeploymentRequestContentType);
            
            _logger.LogTrace($"Start request to {request.RequestUri}");
            
            var response = await HttpClient.SendAsync(request);

            _logger.LogTrace($"End request to {request.RequestUri}");

            return response;
        }
        
        private async Task<(HttpStatusCode, TResponseBody)> Do<TResponseBody>(Uri target, HttpMethod method, object model) where TResponseBody : new()
        {
            var response = await DoInternal(target, method, model);

            try
            {
                var bodyStream = await response.Content.ReadAsStreamAsync();
                
                using (var reader = new StreamReader(bodyStream))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    var serializedBody = (TResponseBody)serializer.Deserialize(reader, typeof(TResponseBody));
                    return (response.StatusCode, serializedBody);
                }
            }
            catch (Exception e)
            {
                switch (e)
                {
                    case JsonSerializationException serializationException:
                        _logger.LogError(serializationException, $"Unable to serialize Html response to type {typeof(TResponseBody).FullName}");
                        throw new Exception($"Unable to serialize Html response to type {typeof(TResponseBody).FullName}", serializationException);
                }
                
                throw;
            }
        }
        
        private async Task<HttpStatusCode> Do(Uri target, HttpMethod method, object model) 
        {
            var response = await DoInternal(target, method, model);

            return response.StatusCode;
        }

        public virtual Task<HttpStatusCode> DoPost(Uri target, object model)
        {
            return Do(target, HttpMethod.Post, model);
        }

        public virtual Task<(HttpStatusCode, TResponseBody)> DoPostWithResponse<TResponseBody>(Uri target, object model) where TResponseBody : new()
        {
            return Do<TResponseBody>(target, HttpMethod.Post, model);
        }

        public Task<HttpStatusCode> DoGet(Uri target)
        {
            return Do(target, HttpMethod.Get, null);
        }

        public Task<(HttpStatusCode, TResponseBody)> DoGetWithResponse<TResponseBody>(Uri target) where TResponseBody : new()
        {
            return Do<TResponseBody>(target, HttpMethod.Get, null);
        }

        public Task<HttpStatusCode> DoPut(Uri target, object model)
        {
            return Do(target, HttpMethod.Put, model);
        }

        public Task<(HttpStatusCode, TResponseBody)> DoPutWithResponse<TResponseBody>(Uri target, object model) where TResponseBody : new()
        {
            return Do<TResponseBody>(target, HttpMethod.Put, model);
        }
    }
}