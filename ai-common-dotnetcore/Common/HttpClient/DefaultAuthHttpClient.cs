﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Samhammer.DependencyInjection.Attributes;

namespace AiCommon.Common.HttpClient
{
    [InjectAs(typeof(IAuthHttpClient), ServiceLifetime.Transient)]
    public class DefaultAuthHttpClient : DefaultHttpClient, IAuthHttpClient
    {
        private readonly ILogger<DefaultAuthHttpClient> _logger;
        
        public DefaultAuthHttpClient(ILogger<DefaultAuthHttpClient> logger): base(logger)
        {
            _logger = logger;
        }
        
        private async Task<HttpResponseMessage> DoInternal(Uri target, HttpMethod method, object model, string rawWebToken)
        {
            var request = new HttpRequestMessage
            {
                RequestUri = target,
                Method = method,
            };
            
            if(!string.IsNullOrEmpty(rawWebToken))
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", rawWebToken);
            
            return await base.DoInternal(request, model);
        }
        
        private async Task<(HttpStatusCode, TResponseBody)> Do<TResponseBody>(Uri target, HttpMethod method, object model, string rawWebToken) where TResponseBody : new()
        {
            var response = await DoInternal(target, method, model, rawWebToken);

            try
            {
                _logger.LogDebug($"Raw response to [uri:{target}, method:{method.Method}]: '{response.Content.ReadAsStringAsync().Result}'");
                
                var bodyStream = await response.Content.ReadAsStreamAsync();
                
                using (var reader = new StreamReader(bodyStream))
                {
                    var serializer = new JsonSerializer();
                    var serializedBody = (TResponseBody)serializer.Deserialize(reader, typeof(TResponseBody));
                    return (response.StatusCode, serializedBody);
                }
            }
            catch (Exception e)
            {
                switch (e)
                {
                    case JsonSerializationException serializationException:
                        _logger.LogError(serializationException, $"Unable to serialize Html response to type {typeof(TResponseBody).FullName}");
                        throw new Exception($"Unable to serialize Html response to type {typeof(TResponseBody).FullName}", serializationException);
                }
                
                throw;
            }
        }
        
        private async Task<HttpStatusCode> Do(Uri target, HttpMethod method, object model, string rawWebToken) 
        {
            var response = await DoInternal(target, method, model, rawWebToken);

            return response.StatusCode;
        }

        public virtual Task<HttpStatusCode> DoPost(Uri target, object model, string rawWebToken)
        {
            return Do(target, HttpMethod.Post, model, rawWebToken);
        }

        public virtual Task<(HttpStatusCode, TResponseBody)> DoPostWithResponse<TResponseBody>(Uri target, object model, string rawWebToken) where TResponseBody : new()
        {
            return Do<TResponseBody>(target, HttpMethod.Post, model, rawWebToken);
        }

        public Task<HttpStatusCode> DoGet(Uri target, string rawWebToken)
        {
            return Do(target, HttpMethod.Get, null, rawWebToken);
        }

        public Task<(HttpStatusCode, TResponseBody)> DoGetWithResponse<TResponseBody>(Uri target, string rawWebToken) where TResponseBody : new()
        {
            return Do<TResponseBody>(target, HttpMethod.Get, null, rawWebToken);
        }

        public Task<HttpStatusCode> DoPut(Uri target, object model, string rawWebToken)
        {
            return Do(target, HttpMethod.Put, model, rawWebToken);
        }

        public Task<(HttpStatusCode, TResponseBody)> DoPutWithResponse<TResponseBody>(Uri target, object model, string rawWebToken) where TResponseBody : new()
        {
            return Do<TResponseBody>(target, HttpMethod.Put, model, rawWebToken);
        }
    }
}