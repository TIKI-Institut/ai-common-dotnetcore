﻿using System.Net.Http.Headers;
using AiCommon.Common.Auth.Context;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Samhammer.DependencyInjection.Attributes;

namespace AiCommon.Common.HttpClient
{
    [InjectAs(typeof(IHttpClient), ServiceLifetime.Transient)]
    public class DefaultAuthWrappingHttpClient : DefaultHttpClient
    {
        private readonly DspOauthContext _authContext;

        public DefaultAuthWrappingHttpClient(ILogger<DefaultAuthWrappingHttpClient> logger, DspOauthContext authContext) : base(logger)
        {
            _authContext = authContext;
        }


        protected override System.Net.Http.HttpClient HttpClient
        {
            get
            {
                var client = base.HttpClient;
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _authContext.Token.GetRaw());
                return client;
            }
        }
    }
}