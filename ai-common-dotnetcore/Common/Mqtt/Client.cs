using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MQTTnet;
using MQTTnet.Extensions.ManagedClient;
using MQTTnet.Protocol;
using Newtonsoft.Json;

namespace AiCommon.Common.Mqtt
{
    public class Client : IMqttClient
    {
        private readonly ILogger<IMqttClient> _logger;
        private readonly ClientMessageReceiver _messageReceiver;
        private readonly IManagedMqttClient _mqttClient;

        private readonly IManagedMqttClientOptions _options;

        internal Client(IManagedMqttClientOptions options, ILogger<IMqttClient> logger)
        {
            _options = options;

            var factory = new MqttFactory();
            _mqttClient = factory.CreateManagedMqttClient();
            _messageReceiver = new ClientMessageReceiver(_mqttClient, logger);
            _mqttClient.UseApplicationMessageReceivedHandler(_messageReceiver);
            _logger = logger;
        }

        public string ClientId() => _options.ClientOptions.ClientId;

        public Task Connect()
        {
            if (_mqttClient.IsStarted)
                return Task.CompletedTask;

            _mqttClient.ConnectingFailedHandler = new ConnectionFailedHandler();

            var connectingTask = _mqttClient.StartAsync(_options);

            return connectingTask;
        }

        public Task Disconnect()
        {
            for (var i = 0; i < 5 && _mqttClient.PendingApplicationMessagesCount > 0; i++)
            {
                _logger.LogWarning(
                    $"There are {_mqttClient.PendingApplicationMessagesCount} pending messages on disconnect. Waiting 5 sec ({i + 1} of 5) ...");
                Thread.Sleep(5000);
            }

            _logger.LogDebug("No pending messages on disconnect. Proceeding with disconnect...");
            return _mqttClient.StopAsync();
        }


        public Task Publish<TObject>(string topic, TObject payload, MqttQualityOfServiceLevel qosLevel)
        {
            return _mqttClient.PublishAsync(topic, JsonConvert.SerializeObject(payload), qosLevel);
        }

        public Task Publish(string topic, string payload, MqttQualityOfServiceLevel qosLevel)
        {
            return _mqttClient.PublishAsync(topic, $"\"{payload}\"", qosLevel);
        }

        public Task Publish(string topic, MqttQualityOfServiceLevel qosLevel)
        {
            return _mqttClient.PublishAsync(topic, null, qosLevel);
        }

        public Guid Subscribe<TPayload>(string topic, MqttQualityOfServiceLevel qos,
            Func<string, TPayload, Exception, Task> handler) where TPayload : class
        {
            return _messageReceiver.Subscribe(topic, qos, handler);
        }

        public Guid Subscribe(string topic, MqttQualityOfServiceLevel qos, Func<string, Exception, Task> handler)
        {
            return _messageReceiver.Subscribe(topic, qos, handler);
        }

        public void Unsubscribe(Guid subscriptionId)
        {
            _messageReceiver.Unsubscribe(subscriptionId);
        }

        private class ConnectionFailedHandler : IConnectingFailedHandler
        {
            public Task HandleConnectingFailedAsync(ManagedProcessFailedEventArgs eventArgs)
            {
                return Task.FromException(eventArgs.Exception);
            }
        }
    }
}