using System;
using AiCommon.Common.Kubernetes;
using Microsoft.Extensions.Logging;
using Samhammer.DependencyInjection.Attributes;

namespace AiCommon.Common.Mqtt
{
    public interface ILocalNamespaceBrokerClientProvider
    {
        IMqttClient CreateClient(string clientIdSuffix);
    }
    
    [InjectAs(typeof(ILocalNamespaceBrokerClientProvider))]
    public class LocalNamespaceBrokerClientProvider : ILocalNamespaceBrokerClientProvider
    {
        private readonly ILogger<LocalNamespaceBrokerClientProvider> _logger;
        private readonly ILogger<IMqttClient> _mqttClientLogger;
        private readonly ILocalNamespaceBrokerClientConfigProvider _localNamespaceBrokerClientConfigProvider;
        private readonly IPodReference _currentPodReference;

        public LocalNamespaceBrokerClientProvider(ILogger<LocalNamespaceBrokerClientProvider> logger,
            ILogger<IMqttClient> mqttClientLogger,
            ILocalNamespaceBrokerClientConfigProvider localNamespaceBrokerClientConfigProvider,
            // ReSharper disable once SuggestBaseTypeForParameter
            ICurrentPodReference currentPodReference)
        {
            _logger = logger;
            _localNamespaceBrokerClientConfigProvider = localNamespaceBrokerClientConfigProvider;
            _currentPodReference = currentPodReference;
            _mqttClientLogger = mqttClientLogger;
        }

        public IMqttClient CreateClient(string clientIdSuffix)
        {
            if (string.IsNullOrWhiteSpace(clientIdSuffix))
                throw new ArgumentException("invalid value (must not be null or empty or whitespaces)", nameof(clientIdSuffix));

            var mqttClientConfig = _localNamespaceBrokerClientConfigProvider.MqttClientConfig;
            var newClientId = $"{_currentPodReference.Pod}-{clientIdSuffix}";
            
            _logger.LogDebug($"Changed clientId to {newClientId} so that it include the current pod name");
            mqttClientConfig.ClientId = newClientId;

            return ClientFactory.NewClient(mqttClientConfig, _mqttClientLogger);
        }
    }
}
