using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Samhammer.DependencyInjection.Attributes;

namespace AiCommon.Common.Mqtt
{
    public interface ILocalNamespaceBrokerClientConfigProvider
    {
        MqttClientConfig MqttClientConfig { get; }
    }
    
    [InjectAs(typeof(ILocalNamespaceBrokerClientConfigProvider), ServiceLifetime.Singleton)]
    public class LocalNamespaceBrokerClientConfigProvider : ILocalNamespaceBrokerClientConfigProvider
    {
        private readonly ILogger<LocalNamespaceBrokerClientConfigProvider> _logger;
        private const string NamespaceBrokerPort = "NAMESPACE_BROKER_SERVICE_PORT";

        public LocalNamespaceBrokerClientConfigProvider(ILogger<LocalNamespaceBrokerClientConfigProvider> logger)
        {
            _logger = logger;
        }

        public MqttClientConfig MqttClientConfig 
        {
            get
            {
                //This uses the K8s provided information to connect to my local namespace broker
                var brokerPort = Environment.GetEnvironmentVariable(NamespaceBrokerPort);
                var localNamespaceBroker = "tcp://namespace-broker";

                if (!string.IsNullOrEmpty(brokerPort))
                {
                    localNamespaceBroker = $"{localNamespaceBroker}:{brokerPort}";
                }
                else
                {
                    _logger.LogWarning($"env var {NamespaceBrokerPort} is not set, namespace broker uri will be set to: {localNamespaceBroker}");
                }

                return new MqttClientConfig {Broker = localNamespaceBroker};
            }
        }
    }
}