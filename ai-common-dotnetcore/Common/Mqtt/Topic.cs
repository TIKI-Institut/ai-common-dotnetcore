using System;
using System.Collections.Generic;
using System.Linq;

namespace AiCommon.Common.Mqtt
{
    
    public class Topic : List<string>
    {
        internal const string MultiLevelWildcard = "#" ;
        internal const string SingleLevelWildcard = "+" ;
        private const string TopicLevelSeparator = "/" ;

        public Topic(string s) : base(s.Split(TopicLevelSeparator))
        {
            Validate(this);
        }

        private static void Validate(List<string> topic)
        {
            if (topic.Count == 0)
                throw new ArgumentException($"please provide non-empty topic");

            var empty = 0;
            var multi = 0;

            foreach (var elem in topic)
            {
                switch (elem)
                {
                    case "":
                        empty++;
                        break;
                    case MultiLevelWildcard:
                        multi++;
                        break;
                }
            }

            if (multi > 1 || empty > 0 || (multi == 1 && topic.Last() != MultiLevelWildcard))
            {
                throw new ArgumentException($"provided topic: [{topic}] is invalid");
            }
        }

        public override string ToString()
        {
            return string.Join(TopicLevelSeparator, this);
        }

        public bool Covers(Topic other)
        {
            if (other == null) return false;

            if (other.Count < Count) return false;
            
            for (var i = 0; i < Count; i++)
            {
                switch (this[i])
                {
                    case Topic.MultiLevelWildcard:
                        return true;
                    case Topic.SingleLevelWildcard:
                        continue;
                }

                if (this[i] != other[i])
                    return false;
            }

            return Count == other.Count;
        }
    }
}