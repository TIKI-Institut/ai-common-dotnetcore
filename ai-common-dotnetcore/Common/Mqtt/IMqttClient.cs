using System;
using System.Threading.Tasks;
using MQTTnet.Protocol;

namespace AiCommon.Common.Mqtt
{
    
    public interface IClientMessageReceiver
    {
        Guid Subscribe<TPayload>(string topic, MqttQualityOfServiceLevel qos, Func<string, TPayload, Exception, Task> handler) where TPayload : class;
        Guid Subscribe(string topic, MqttQualityOfServiceLevel qos, Func<string, Exception, Task> handler) ;
        void Unsubscribe(Guid subscriptionId);
    } 
    
    public interface IMqttClient : IClientMessageReceiver
    {
        string ClientId();
        Task Connect();
        Task Disconnect();
        Task Publish<TObject>(string topic, TObject payload, MqttQualityOfServiceLevel qosLevel);
        Task Publish(string topic, string payload, MqttQualityOfServiceLevel qosLevel);
        Task Publish(string topic, MqttQualityOfServiceLevel qosLevel);
    }
}
