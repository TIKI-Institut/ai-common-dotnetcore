using System;

namespace AiCommon.Common.Mqtt
{
    public class MqttTlsClientConfig
    {
        public string ServerCaFile { get; set; }
        public string ClientPfxCertFile { get; set; }
    }
    
    public class MqttClientConfig
    {
        public string Broker { get; set; }
        public string ClientId { get; set; }
        public MqttTlsClientConfig MqttTlsClientConfig { get; set; }
    }
}