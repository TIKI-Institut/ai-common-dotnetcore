using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MQTTnet;
using MQTTnet.Client.Receiving;
using MQTTnet.Extensions.ManagedClient;
using MQTTnet.Protocol;
using Newtonsoft.Json;

namespace AiCommon.Common.Mqtt
{
    public class ClientMessageReceiver : IClientMessageReceiver, IMqttApplicationMessageReceivedHandler
    {
        interface ISubscriptionEntry
        {
            Guid SubscriptionId { get; }
            Task DispatchMessage(MqttApplicationMessage msg);
        }

        private readonly ILogger _logger;
        private readonly IManagedMqttClient _mqttClient;
        private readonly ConcurrentDictionary<Guid, (Topic, ISubscriptionEntry)> _subscriptions;
        private readonly ConcurrentDictionary<string, uint> _subscriptionCounts;

        public ClientMessageReceiver(IManagedMqttClient mqttClient, ILogger logger)
        {
            _logger = logger;
            _mqttClient = mqttClient ?? throw new ArgumentException("argument is null", nameof(mqttClient));
            _subscriptions = new ConcurrentDictionary<Guid, (Topic,ISubscriptionEntry)>();
            _subscriptionCounts = new ConcurrentDictionary<string, uint>();
        }

        public Task HandleApplicationMessageReceivedAsync(MqttApplicationMessageReceivedEventArgs eventArgs)
        {
            var rawMessageTopic = eventArgs.ApplicationMessage.Topic;
            var messageTopic = new Topic(rawMessageTopic);

            return Task
                .WhenAll(_subscriptions
                    .Where(x => x.Value.Item1.Covers(messageTopic))
                    .Select(x =>
                    {
                        return x.Value.Item2.DispatchMessage(eventArgs.ApplicationMessage)
                            //todo this is a workaround for the issue with mqtt implementation in dotnet that causes mqtt to crash if
                            // topic handler throws exception that cause handler Task to fail
                            .ContinueWith(t =>
                            {
                                // if task did not end with exception, return it
                                if (t.Exception == null) return t;

                                // if there was Exception during dispatch, log it but still return CompletedTask
                                _logger.LogError(
                                    $"A message sent by client: {eventArgs.ClientId} could not be dispatched (see following log entries for the cause) to topic: {eventArgs.ApplicationMessage.Topic} and will be skipped...");

                                if (_logger.IsEnabled(LogLevel.Debug))
                                {
                                    foreach (var ie in t.Exception.InnerExceptions)
                                    {
                                        _logger.LogError(ie.Message);
                                    }
                                }

                                // swallow exception
                                if (!_logger.IsEnabled(LogLevel.Trace)) return Task.CompletedTask;

                                foreach (var ie in t.Exception.InnerExceptions)
                                {
                                    _logger.LogError(ie.StackTrace);
                                }
                                return Task.CompletedTask;
                            });
                    }));
        }

        public Guid Subscribe<TPayload>(string rawTopic, MqttQualityOfServiceLevel qos, Func<string, TPayload, Exception, Task> handler) where TPayload : class
        {
            var subTask = _mqttClient.SubscribeAsync(rawTopic, qos).ContinueWith(t =>
            {
                var subscription = new TypedMessageDispatcher<TPayload>(handler, _logger);
                var topic = new Topic(rawTopic);
                if (_subscriptions.TryAdd(subscription.SubscriptionId, (topic, subscription)))
                {
                    // to be in sync with later access below vvvvvvvvvvvv
                    _subscriptionCounts.AddOrUpdate(topic.ToString(), s => 1, (s, c) => ++c);
                }

                return subscription.SubscriptionId;
            }, TaskContinuationOptions.OnlyOnRanToCompletion);

            subTask.Wait();

            return subTask.Result;
        }

        public Guid Subscribe(string rawTopic, MqttQualityOfServiceLevel qos, Func<string, Exception, Task> handler)
        {
            var subTask = _mqttClient.SubscribeAsync(rawTopic, qos).ContinueWith(t =>
            {
                var subscription = new EmptyMessageDispatcher(handler, _logger);
                var topic = new Topic(rawTopic);
                if (_subscriptions.TryAdd(subscription.SubscriptionId, (topic, subscription)))
                {
                    // to be in sync with later access below vvvvvvvvvvvv
                    _subscriptionCounts.AddOrUpdate(topic.ToString(), s => 1, (s, c) => ++c);
                }

                return subscription.SubscriptionId;
            }, TaskContinuationOptions.OnlyOnRanToCompletion);

            subTask.Wait();

            return subTask.Result;            
        }

        public void Unsubscribe(Guid subscriptionId)
        {

            if (_subscriptions.TryRemove(subscriptionId, out var sub))
            {
                // to be in sync with access above   vvvvvvvvvvvvvvvvvvvv
                _subscriptionCounts.AddOrUpdate(sub.Item1.ToString(),
                    x => throw new ArgumentException("topic is not tracked"),
                    (t, cnt) =>
                    {
                        if ((--cnt) == 0)
                        {
                            var unsubscribeTask = _mqttClient.UnsubscribeAsync(t);
                            unsubscribeTask.Wait();
                        }

                        return cnt;
                    });
            }
        }

        class TypedMessageDispatcher<T> : ISubscriptionEntry where T : class
        {
            private readonly ILogger _logger;
            private readonly Func<string, T, Exception, Task> _handler;

            public TypedMessageDispatcher(Func<string, T, Exception, Task> handler, ILogger logger)
            {
                _handler = handler;
                SubscriptionId = Guid.NewGuid();
                _logger = logger;
            }

            public Guid SubscriptionId { get; }

            public Task DispatchMessage(MqttApplicationMessage msg)
            {
                _logger.LogTrace($"Received a message on topic: {msg.Topic}");

                try
                {
                    var payload =
                        JsonConvert.DeserializeObject<T>(
                            System.Text.Encoding.UTF8.GetString(msg.Payload, 0, msg.Payload.Length));

                    try
                    {
                        return _handler(msg.Topic, payload, null);
                    }
                    catch (Exception e)
                    {
                        return Task.FromException(e);
                    }

                }
                catch (Exception e)
                {
                    return _handler(msg.Topic, null, e);
                }
            }
        }
        
        class EmptyMessageDispatcher : ISubscriptionEntry 
        {
            private readonly Func<string, Exception, Task> _handler;
            private readonly ILogger _logger;

            public EmptyMessageDispatcher(Func<string, Exception, Task> handler, ILogger logger)
            {
                _handler = handler;
                SubscriptionId = Guid.NewGuid();
                _logger = logger;
            }

            public Guid SubscriptionId { get; }

            public Task DispatchMessage(MqttApplicationMessage msg)
            {
                _logger.LogDebug($"Received an empty message on topic: {msg.Topic}");

                try
                {
                    return _handler(msg.Topic, null);
                }
                catch (Exception e)
                {
                    return Task.FromException(e);
                }

            }
        }
    }
}