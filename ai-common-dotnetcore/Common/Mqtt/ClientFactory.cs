using System;
using System.IO;
using System.Linq;
using System.Net.Security;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using Microsoft.Extensions.Logging;
using MQTTnet.Client.Options;
using MQTTnet.Extensions.ManagedClient;
using MQTTnet.Formatter;

namespace AiCommon.Common.Mqtt
{
    public static class ClientFactory
    {
        public static IMqttClient NewClient(MqttClientConfig config, ILogger<IMqttClient> logger)
        {
            logger.LogDebug($"Mqtt ClientFactory about to produce client: {config.ClientId} for " +
                            $"the broker: {config.Broker}");

            // Create TCP based options using the builder.
            var optionsBuilder = new MqttClientOptionsBuilder()
                .WithClientId(config.ClientId)
                .WithTcpServer(settings =>
                {
                    var target = new Uri(config.Broker);
                    settings.Port = target.Port;
                    settings.Server = target.Host;
                })
                .WithProtocolVersion(MqttProtocolVersion.V311)
                .WithCleanSession();

            if (config.MqttTlsClientConfig != null)
            {
                logger.LogDebug($"TLS config is available for client: {config.ClientId} [ca: " +
                                $"{config.MqttTlsClientConfig.ServerCaFile}; pfx: {config.MqttTlsClientConfig.ClientPfxCertFile}]");

                if (!File.Exists(config.MqttTlsClientConfig.ServerCaFile))
                    throw new FileNotFoundException(
                        $"Server CA Certificate file was not found under: {config.MqttTlsClientConfig.ServerCaFile}");
                if (!File.Exists(config.MqttTlsClientConfig.ClientPfxCertFile))
                    throw new FileNotFoundException(
                        $"Client PFX Certificate file was not found under: {config.MqttTlsClientConfig.ClientPfxCertFile}");

                var serverCaCert = new X509Certificate2(config.MqttTlsClientConfig.ServerCaFile);
                var clientLoginCert = new X509Certificate2(config.MqttTlsClientConfig.ClientPfxCertFile, (string) null,
                    X509KeyStorageFlags.Exportable);

                var tlsOptions = new MqttClientOptionsBuilderTlsParameters
                {
                    UseTls = true,
                    SslProtocol = SslProtocols.Tls12,
                    Certificates = new[]{ clientLoginCert, serverCaCert },
                    AllowUntrustedCertificates = true,
                    CertificateValidationCallback = (certificate, chain, sslPolicyErrors, clientOptions) =>
                    {
                        //Proper CA Cert checking
                        try
                        {
                            if (sslPolicyErrors == SslPolicyErrors.None)
                            {
                                logger.LogDebug("Certificate validation successful");
                                return true;
                            }

                            if ((sslPolicyErrors & SslPolicyErrors.RemoteCertificateChainErrors) != 0)
                            {
                                chain.ChainPolicy.ExtraStore.Add(serverCaCert);
                                chain.Build((X509Certificate2) certificate);

                                var isValid = chain.ChainElements.OfType<X509ChainElement>().Any(a =>
                                    a.Certificate.Thumbprint == serverCaCert.Thumbprint);

                                if (!isValid)
                                {
                                    logger.LogWarning("Certificate validation failed: RemoteCertificateChainErrors");
                                }

                                return isValid;
                            }

                            if ((sslPolicyErrors & SslPolicyErrors.RemoteCertificateNameMismatch) != 0)
                            {
                                logger.LogWarning("Certificate validation failed: RemoteCertificateNameMismatch");
                            } 
                            else if ((sslPolicyErrors & SslPolicyErrors.RemoteCertificateNotAvailable) != 0)
                            {
                                logger.LogWarning("Certificate validation failed: RemoteCertificateNotAvailable");
                            }
                            else
                            {
                                logger.LogWarning($"Certificate validation failed: {nameof(sslPolicyErrors)}");
                                throw new ArgumentOutOfRangeException(nameof(sslPolicyErrors), sslPolicyErrors,
                                    "Certificate validation failed");
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.LogError(ex,
                                "Error during construction of MQTT Client while validating certificates.");
                        }

                        return false;
                    }
                };

                optionsBuilder.WithTls(tlsOptions);
            }
            else
            {
                logger.LogDebug($"No TLS config is available for client {config.ClientId}");
            }

            var options = new ManagedMqttClientOptionsBuilder()
                .WithClientOptions(optionsBuilder.Build())
                .WithAutoReconnectDelay(TimeSpan.FromSeconds(5))
                .Build();

            return new Client(options, logger);
        }
    }
}