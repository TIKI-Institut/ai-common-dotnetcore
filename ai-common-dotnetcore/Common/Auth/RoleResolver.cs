﻿using System.Collections.Generic;
using System.Security.Claims;
using Newtonsoft.Json;

namespace AiCommon.Common.Auth
{
	public static class RoleResolver
	{
		/// <summary>
		/// Gets all roles of the running client (audience).
		/// </summary>
		/// <remarks>
		/// The resource_access inside a token looks like this:
		/// {
		///		"clientId1": {
		///			"roles": ["role1", "role2"]
		///		},
		///		"clientId2": {
		///			"roles": ["role3", "role4"]
		/// 	}
		/// }
		/// </remarks>
		/// <param name="principal">The current principal</param>
		/// <param name="clientId">The id if a client</param>
		/// <returns>A array of roles</returns>
		public static string[] GetRoles(ClaimsPrincipal principal, string clientId)
		{
			var retVal = new string[] { };

			var claimsIdentity = (ClaimsIdentity)principal.Identity;

			if (claimsIdentity.IsAuthenticated && claimsIdentity.HasClaim(claim => claim.Type == "resource_access"))
			{
				var resourceAccessClaim = claimsIdentity.FindFirst(claim => claim.Type == "resource_access");
				var resourceAccess = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, string[]>>>(resourceAccessClaim.Value);

				if (resourceAccess.TryGetValue(clientId, out var clientResourceAccess)
					&& clientResourceAccess.TryGetValue("roles", out var roles))
				{
					retVal = roles;
				}
			}

			return retVal;
		}
	}
}
