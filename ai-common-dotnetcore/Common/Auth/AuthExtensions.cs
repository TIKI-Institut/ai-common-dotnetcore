﻿using AiCommon.Common.Auth.Context;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.DependencyInjection;


namespace AiCommon.Common.Auth
{

	public static class AuthExtensions
	{
		public static IServiceCollection RegisterAuthContext(this IServiceCollection serviceCollection)
		{
            //This is done manually bcs we dont want to register "ClaimsTransformer" only when using AuthContext
            serviceCollection.AddScoped<IClaimsTransformation, ClaimsTransformer>();
            serviceCollection.AddScoped<DspOauthContext>();
            
            return serviceCollection;
		}
    }
}
