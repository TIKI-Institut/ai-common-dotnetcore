﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.DependencyInjection;

namespace AiCommon.Common.Auth
{
    /// <summary>
    /// Microsoft Identity model does not support keycloaks realm_access.
    /// Add all roles to a flat list in claimsIdentity.
    /// </summary>
    public class ClaimsTransformer : IClaimsTransformation
    {
        private readonly IServiceProvider _serviceProvider;

        public ClaimsTransformer(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public Task<ClaimsPrincipal> TransformAsync(ClaimsPrincipal principal)
        {
            return Task.Run(() =>
            {
                var jwtIssuerContext = _serviceProvider.GetService<IJwtIssuerContext>();

                var claimsIdentity = (ClaimsIdentity) principal.Identity;

                foreach (var role in RoleResolver.GetRoles(principal, jwtIssuerContext.ClientId))
                {
                    claimsIdentity.AddClaim(new Claim(ClaimTypes.Role, role));
                }

                return principal;
            });
        }
    }
}