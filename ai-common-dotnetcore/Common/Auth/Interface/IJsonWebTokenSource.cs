﻿
namespace AiCommon.Common.Auth.Interface
{
    public interface IJsonWebTokenSource
    {
        string GetRaw();
    }
}
