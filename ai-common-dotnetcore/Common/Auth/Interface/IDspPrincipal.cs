﻿using System.Collections.Generic;
using System.Security.Principal;

namespace AiCommon.Common.Auth.Interface
{
    public interface IDspPrincipal : IPrincipal
    {
        List<string> GetRoles(string clientId);

        string GetClaimAttribute(string key);

        string GetPrincipalName();
    }
}