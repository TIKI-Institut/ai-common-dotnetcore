﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Protocols;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.IdentityModel.Tokens;

namespace AiCommon.Common.Auth
{
    public interface IJwtIssuerContext
    {
        string Issuer { get; }
        string ClientId { get; }
    }

    class EnvironmentalJwtIssuerContext : IJwtIssuerContext
    {
        public string Issuer => Environment.GetEnvironmentVariable("JWT_ISSUER");
        public string ClientId => Environment.GetEnvironmentVariable("OAUTH_CLIENT_ID");
        public bool IsConfigured => !string.IsNullOrEmpty(Issuer) && !string.IsNullOrEmpty(ClientId);
    }

    public static class JwtIssuerContext
    {
        public static async Task<ICollection<SecurityKey>> ResolveSigningKeys(string issuer)
        {
            var configurationManager = new ConfigurationManager<OpenIdConnectConfiguration>(
                $"{issuer}/.well-known/openid-configuration", new OpenIdConnectConfigurationRetriever());

            var config = await configurationManager.GetConfigurationAsync(CancellationToken.None);

            return config.SigningKeys;
        }

        public static TokenValidationParameters DefaultTokenValidationParameters(IJwtIssuerContext issuerContext)
        {
            return new TokenValidationParameters
            {
                ValidateAudience = true,
                ValidateIssuer = true,
                ValidAudience = issuerContext.ClientId,
                ValidIssuer = issuerContext.Issuer,
                IssuerSigningKeys = ResolveSigningKeys(issuerContext.Issuer).Result
            };
        }
    }
}