﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using AiCommon.Common.Auth.Interface;

namespace AiCommon.Common.Auth.Context
{
    public class DspPrincipal : ClaimsPrincipal, IDspPrincipal
    {
        public DspPrincipal(IPrincipal principal) : base(principal)
        {
        }

        public List<string> GetRoles(string clientId)
        {
            return RoleResolver.GetRoles(this, clientId).ToList();
        }

        public string GetClaimAttribute(string key)
        {
            return Claims.SingleOrDefault(c => c.Type.Equals(key))?.Value;
        }

        public string GetPrincipalName()
        {
            return GetClaimAttribute("iss")?.Split('/').Last();
        }
    }
}
