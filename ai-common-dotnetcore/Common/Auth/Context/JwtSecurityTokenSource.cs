﻿using System.IdentityModel.Tokens.Jwt;
using AiCommon.Common.Auth.Interface;

namespace AiCommon.Common.Auth.Context
{
    public class JwtSecurityTokenSource : IJsonWebTokenSource
    {
        private readonly JwtSecurityToken _token;

        public JwtSecurityTokenSource(JwtSecurityToken token)
        {
            _token = token;
        }

        public string GetRaw()
        {
            return _token.RawData;
        }
    }
}