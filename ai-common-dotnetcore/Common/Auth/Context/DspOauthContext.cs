﻿using AiCommon.Common.Auth.Interface;

namespace AiCommon.Common.Auth.Context
{
    public class DspOauthContext
    {
        public IJsonWebTokenSource Token { get; set; }
        public IDspPrincipal Principal { get; set; }
    }
}