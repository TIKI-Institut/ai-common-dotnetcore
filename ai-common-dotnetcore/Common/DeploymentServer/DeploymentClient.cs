﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using AiCommon.Common.HttpClient;
using AiCommon.Common.Kubernetes;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Samhammer.DependencyInjection.Attributes;

namespace AiCommon.Common.DeploymentServer
{
    [JsonObject(MemberSerialization.OptIn)]
    public class DeploymentOverride
    {
        [JsonProperty(PropertyName = "args")] public IEnumerable<string> Arguments { get; set; }
        [JsonProperty(PropertyName = "envVars")] public IEnumerable<string> EnvVars { get; set; }
        [JsonProperty("namePostfix")] public string NamePostfix { get; set; }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class ResponseMessage
    {
        [JsonProperty("msg")] 
        public string Msg { get; set; }
    }

    public interface IDeploymentClient
    {
        Task<(HttpStatusCode, TResponseBody)> Deploy<TResponseBody>(IFlavorReference target, string rawWebToken) where TResponseBody : new();
        Task<(HttpStatusCode, TResponseBody)> Deploy<TResponseBody>(IFlavorReference target, DeploymentOverride[] deploymentOverrides, string rawWebToken) where TResponseBody : new();

        Task<(HttpStatusCode, ResponseMessage)> Deploy(IFlavorReference target, string rawWebToken);
        Task<(HttpStatusCode, ResponseMessage)> Deploy(IFlavorReference target, DeploymentOverride[] deploymentOverrides, string rawWebToken);
    }

    [InjectAs(typeof(IDeploymentClient))]
    public class DeploymentClient : IDeploymentClient
    {
        private readonly ILogger<DeploymentClient> _logger;
        private readonly DeploymentClientConfig _clientConfig;
        private readonly IAuthHttpClient _httpClient;

        public DeploymentClient(IOptions<DeploymentClientConfig> deploymentServerConfig, 
            IAuthHttpClient httpClient, ILogger<DeploymentClient> logger)
        {
            _logger = logger;
            _clientConfig = deploymentServerConfig.Value;
            _httpClient = httpClient;
        }

        private Uri ApplyFlavor(string purpose, IFlavorReference flavorReference)
        {
            var builder = new UriBuilder(_clientConfig.ServerUrl);
            builder.Path += builder.Path.EndsWith("/") ? "" : "/";
            builder.Path += $"{purpose}/{flavorReference.Principal}/{flavorReference.Name}/{flavorReference.Environment}/{flavorReference.FlavorName}";
            return builder.Uri;
        }
        
        private Uri ApplyFlavorVersion(string purpose, IFlavorReference flavorReference)
        {
            var builder = new UriBuilder(ApplyFlavor(purpose, flavorReference));
            builder.Path += $"/{flavorReference.FlavorVersion}";
            return builder.Uri;
        }

        public Task<(HttpStatusCode, ResponseMessage)> Deploy(IFlavorReference target, string rawWebToken)
        {
            return Deploy<ResponseMessage>(target, null, rawWebToken);
        }

        public Task<(HttpStatusCode, ResponseMessage)> Deploy(IFlavorReference target, DeploymentOverride[] deploymentOverrides, string rawWebToken)
        {
            return Deploy<ResponseMessage>(target, deploymentOverrides, rawWebToken);
        }

        public Task<(HttpStatusCode, TResponseBody)> Deploy<TResponseBody>(IFlavorReference target, string rawWebToken) where TResponseBody : new()
        {
            return Deploy<TResponseBody>(target, null, rawWebToken);
        }

        public Task<(HttpStatusCode, TResponseBody)> Deploy<TResponseBody>(IFlavorReference target, DeploymentOverride[] deploymentOverrides, string rawWebToken) where TResponseBody : new()
        {
            var uri = ApplyFlavorVersion("phase/deploy", target);
            _logger.LogDebug($"POST to deployment server uri: {uri}");
            return _httpClient.DoPostWithResponse<TResponseBody>(uri, deploymentOverrides, rawWebToken);
        }
    }
}