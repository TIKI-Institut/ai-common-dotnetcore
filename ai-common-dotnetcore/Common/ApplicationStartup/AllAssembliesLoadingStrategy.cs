using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using Samhammer.DependencyInjection.Providers;

namespace AiCommon.Common.ApplicationStartup
{
    public class AllAssembliesLoadingStrategy : IAssemblyResolvingStrategy
    {
        private IEnumerable<Assembly> AppDomainAssemblies => AppDomain.CurrentDomain.GetAssemblies(); 
        
        private IEnumerable<Assembly> ReferencedAssemblies()
        {
            Assembly loadedAssembly = null;
            foreach (var dependencyName in Assembly.GetEntryAssembly().GetReferencedAssemblies())
            {
                try
                {
                    // Try to load the referenced assembly...
                    loadedAssembly = Assembly.Load(dependencyName);
                }
                catch
                {
                    // Failed to load assembly. Skip it.
                    loadedAssembly = null;
                }

                if (loadedAssembly != null)
                {
                    yield return loadedAssembly;
                }
            }
        }
        
        public IEnumerable<Assembly> ResolveAssemblies()
        {
            return AppDomainAssemblies.Concat(ReferencedAssemblies()).Distinct();

        }
    }
}