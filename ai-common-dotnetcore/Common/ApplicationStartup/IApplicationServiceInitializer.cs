using Microsoft.Extensions.DependencyInjection;

namespace AiCommon.Common.ApplicationStartup
{
    public interface IApplicationServiceInitializer
    {
        void ConfigureServices(IServiceCollection serviceCollection);
    }
}