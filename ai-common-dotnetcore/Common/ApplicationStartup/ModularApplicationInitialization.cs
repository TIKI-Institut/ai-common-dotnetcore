using System;
using System.Collections.Generic;
using System.Linq;
using AiCommon.Main.Web;
using Microsoft.Extensions.DependencyInjection;

namespace AiCommon.Common.ApplicationStartup
{
     
    public interface IModularApplicationInitialization: IApplicationServiceInitializer
    {
        string ApplicationSelector { get; }
    }


    public static class ModularApplicationInitialization
    {
        public static IServiceCollection UseModularApplicationInitialization(this IServiceCollection serviceCollection, string applicationSelector)
        {

            var currentServiceProvider = serviceCollection.BuildServiceProvider();

            var appStartupServiceInitializers = currentServiceProvider.GetService<IEnumerable<IModularApplicationInitialization>>();


            foreach (var applicationServiceInitializer in appStartupServiceInitializers.Where(x => x.ApplicationSelector == applicationSelector))
            {
                applicationServiceInitializer.ConfigureServices(serviceCollection);
            }

            return serviceCollection;
        }
        
        public static IServiceCollection UseModularApplicationInitialization(this IServiceCollection serviceCollection, Predicate<string> predicate)
        {

            var currentServiceProvider = serviceCollection.BuildServiceProvider();

            var appStartupServiceInitializers = currentServiceProvider.GetService<IEnumerable<IModularApplicationInitialization>>();


            foreach (var applicationServiceInitializer in appStartupServiceInitializers.Where(x => predicate( x.ApplicationSelector)))
            {
                applicationServiceInitializer.ConfigureServices(serviceCollection);
            }

            return serviceCollection;
        }

    }
    
}