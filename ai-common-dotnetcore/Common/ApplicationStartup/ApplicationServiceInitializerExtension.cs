using Microsoft.Extensions.DependencyInjection;

#if NETCOREAPP3_1
using Microsoft.Extensions.Hosting;
#elif NETCOREAPP2_2
using Microsoft.AspNetCore.Hosting;
#endif

namespace AiCommon.Common.ApplicationStartup
{
    public static class ApplicationServiceInitializerExtension
    {
        public static IServiceCollection UseApplicationServiceInitializer(this IServiceCollection serviceCollection, IApplicationServiceInitializer initializer)
        {
            initializer.ConfigureServices(serviceCollection);
            return serviceCollection;
        }

        public static IServiceCollection UseApplicationServiceInitializer<TInit>(this IServiceCollection serviceCollection) where TInit:IApplicationServiceInitializer, new()
        {
            return UseApplicationServiceInitializer(serviceCollection, new TInit());
        }


#if NETCOREAPP3_1
        public static IHostBuilder UseApplicationServiceInitializer(this IHostBuilder builder, IApplicationServiceInitializer initializer)
        {
            builder.ConfigureServices(initializer.ConfigureServices);
            return builder;
        }
        
        public static IHostBuilder UseApplicationServiceInitializer<TInit>(this IHostBuilder builder) where TInit:IApplicationServiceInitializer, new()
        {
            return UseApplicationServiceInitializer(builder, new TInit());
        }
#elif NETCOREAPP2_2
        public static IWebHostBuilder  UseApplicationServiceInitializer(this IWebHostBuilder  builder, IApplicationServiceInitializer initializer)
        {
            builder.ConfigureServices(initializer.ConfigureServices);
            return builder;
        }
        
        public static IWebHostBuilder  UseApplicationServiceInitializer<TInit>(this IWebHostBuilder  builder) where TInit:IApplicationServiceInitializer, new()
        {
            return UseApplicationServiceInitializer(builder, new TInit());
        }
#endif




    }
}