using System.IO;
using System.Linq;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.SystemConsole.Themes;

namespace AiCommon.Common.ApplicationStartup.Initializers
{
    public interface ISerilogConfiguration
    {
        void Init(LoggerConfiguration loggerConfig);
    }

    public class SerilogInitializer : IApplicationServiceInitializer
    {
        public static ISerilogConfiguration Configuration { get; } = new DefaultSerilogConfig();

        public void ConfigureServices(IServiceCollection serviceCollection)
        {
            Log.Logger = ConfigureLogging().CreateLogger();
            serviceCollection
                .AddLogging(x => x.ClearProviders())
                .AddLogging(configure => configure.AddSerilog(Log.Logger).SetMinimumLevel(LogLevel.Debug));
        }

        protected LoggerConfiguration ConfigureLogging(string appSettingsFileName = "appsettings.json")
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile(appSettingsFileName, true, true)
                .Build();

            var loggerConfig = new LoggerConfiguration();
            loggerConfig.ReadFrom.Configuration(configuration);

            if (!configuration.GetSection("Serilog").Exists() ||
                configuration.GetSection("Serilog").GetChildren().All(e => e.Key != "WriteTo"))
            {
                Configuration.Init(loggerConfig); // init default console writer
            }

            return loggerConfig;
        }

        private class DefaultSerilogConfig : ISerilogConfiguration
        {
            public void Init(LoggerConfiguration loggerConfig)
            {
                const string outputTemplate =
                    "[{Level:u3}] {Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} - {SourceContext} ==> {Message:lj}{NewLine}{Exception}";
                loggerConfig.WriteTo.Console(LogEventLevel.Information, outputTemplate, theme: ConsoleTheme.None);
            }
        }
    }
}