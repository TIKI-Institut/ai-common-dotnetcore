using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Samhammer.DependencyInjection.Attributes;

namespace AiCommon.Common.Kubernetes
{
    public interface ICurrentPodReference : IPodReference {}

    [InjectAs(typeof(ICurrentPodReference), ServiceLifetime.Singleton)]
    public class CurrentPodReference : ICurrentPodReference
    {
        private const string DspNamespaceEnv = "DSP_NAMESPACE";
        private const string DspPodEnv = "DSP_POD";

        public CurrentPodReference(IOptionsSnapshot<PodReference> pod, ILogger<CurrentPodReference> logger)
        {
            var currentPodConfig = pod?.Get("current-pod");

            Namespace = currentPodConfig?.Namespace;
            Namespace = PropertyHelper.CheckPropertyWithEnvVarFallback(nameof(Namespace), Namespace, DspNamespaceEnv, logger);

            Pod = currentPodConfig?.Pod;
            Pod = PropertyHelper.CheckPropertyWithEnvVarFallback(nameof(Pod), Pod, DspPodEnv, logger);
        }

        public string Namespace { get; }
        public string Pod { get; }
        
        public override string ToString() => $"{(string.IsNullOrWhiteSpace(Namespace) ? "<unset>" : Namespace)}/{(string.IsNullOrWhiteSpace(Pod) ? "<unset>" : Pod)}";
    }
}