﻿using System.Diagnostics.CodeAnalysis;
using Newtonsoft.Json;

namespace AiCommon.Common.Kubernetes
{
    public interface IFlavorReference
    {
        string Principal { get;  }
        string Name { get;  }
        string Environment { get; }
        string FlavorName { get; }
        string FlavorVersion { get; }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class FlavorReference : IFlavorReference
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("principal")]
        public string Principal { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("environment")]
        public string Environment { get; set; }

        [JsonProperty("flavorName")]
        public string FlavorName { get; set; }

        [JsonProperty("flavorVersion")]
        public string FlavorVersion { get; set; }

        private static string UnsetToString(string val) => $"{(string.IsNullOrWhiteSpace(val) ? "<unset>" : val)}";

        public override string ToString() => $"{UnsetToString(Principal)}/{UnsetToString(Name)}/{UnsetToString(Environment)}" +
                                             $"/{UnsetToString(FlavorName)}/{UnsetToString(FlavorVersion)}";
        
        public static FlavorReference FromFlavorReference(IFlavorReference @ref)
        {
            return new FlavorReference
            {
                Principal = @ref.Principal,
                Name = @ref.Name,
                Environment = @ref.Environment,
                FlavorName = @ref.FlavorName,
                FlavorVersion = @ref.FlavorVersion,
            };
        }

        protected bool Equals(FlavorReference other)
        {
            return Principal == other.Principal && Name == other.Name && Environment == other.Environment && FlavorName == other.FlavorName && FlavorVersion == other.FlavorVersion;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((FlavorReference) obj);
        }

        [SuppressMessage("ReSharper", "NonReadonlyMemberInGetHashCode")]
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Principal != null ? Principal.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Name != null ? Name.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Environment != null ? Environment.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (FlavorName != null ? FlavorName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (FlavorVersion != null ? FlavorVersion.GetHashCode() : 0);
                return hashCode;
            }
        }

        public static bool operator ==(FlavorReference left, FlavorReference right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(FlavorReference left, FlavorReference right)
        {
            return !Equals(left, right);
        }
    }
}
