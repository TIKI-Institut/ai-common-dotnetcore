﻿namespace AiCommon.Common.Kubernetes
{
    public static class Extensions
    {
        public static string Namespace(this IFlavorReference t)
        {
            var retVal = t.Principal;
            if (!string.IsNullOrEmpty(t.Name))
            {
                retVal += $"-{t.Name}";
            }

            if (!string.IsNullOrEmpty(t.Environment))
            {
                retVal += $"-{t.Environment}";
            }
            return retVal;
        }
    }
}