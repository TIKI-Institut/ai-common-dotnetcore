﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Samhammer.DependencyInjection.Attributes;

namespace AiCommon.Common.Kubernetes
{
    public interface ICurrentFlavorReference : IFlavorReference {}
    
    [InjectAs(typeof(ICurrentFlavorReference), ServiceLifetime.Singleton)]
    public class CurrentFlavorReference: ICurrentFlavorReference
    {
        private const string EnvDspPrincipalName = "DSP_PRINCIPAL";
        private const string EnvDspName = "DSP_NAME";
        private const string EnvDspEnvironment = "DSP_ENVIRONMENT";
        private const string EnvDspFlavorName = "DSP_FLAVOR";
        private const string EnvDspFlavorVersion = "DSP_FLAVOR_VERSION";

        public CurrentFlavorReference(IOptionsSnapshot<FlavorReference> flavor, ILogger<CurrentFlavorReference> logger)
        {
            var currentFlavorConfig = flavor?.Get("current-flavor");
            
            Principal = currentFlavorConfig?.Principal;
            Principal = PropertyHelper.CheckPropertyWithEnvVarFallback(nameof(Principal), Principal,
                EnvDspPrincipalName, logger);

            Name = currentFlavorConfig?.Name;
            Name = PropertyHelper.CheckPropertyWithEnvVarFallback(nameof(Name), Name,
                EnvDspName, logger);

            Environment = currentFlavorConfig?.Environment;
            Environment = PropertyHelper.CheckPropertyWithEnvVarFallback(nameof(Environment), Environment,
                EnvDspEnvironment, logger);

            FlavorName = currentFlavorConfig?.FlavorName;
            FlavorName = PropertyHelper.CheckPropertyWithEnvVarFallback(nameof(FlavorName), FlavorName,
                EnvDspFlavorName, logger);

            FlavorVersion = currentFlavorConfig?.FlavorVersion;
            FlavorVersion = PropertyHelper.CheckPropertyWithEnvVarFallback(nameof(FlavorVersion), FlavorVersion,
                EnvDspFlavorVersion, logger);
        }

        public string Principal { get; }
        public string Name { get; }
        public string Environment { get; }
        public string FlavorName { get; }
        public string FlavorVersion { get; }
        
        private static string UnsetToString(string val) => $"{(string.IsNullOrWhiteSpace(val) ? "<unset>" : val)}";

        public override string ToString() => $"{UnsetToString(Principal)}/{UnsetToString(Name)}/{UnsetToString(Environment)}" +
                                             $"/{UnsetToString(FlavorName)}/{UnsetToString(FlavorVersion)}";
    }
}