﻿using System;
using Microsoft.Extensions.Logging;

namespace AiCommon.Common.Kubernetes
{
    internal static class PropertyHelper
    {
        public static string CheckPropertyWithEnvVarFallback(string propertyName, string propertyValue, string fallbackEnvVarName, ILogger logger)
        {
            if (!string.IsNullOrEmpty(propertyValue)) return propertyValue;
            
            logger.LogDebug($"{propertyName} not configured; falling back to ENV '{fallbackEnvVarName}'");
            var envNs = Environment.GetEnvironmentVariable(fallbackEnvVarName);

            if (string.IsNullOrEmpty(envNs))
                logger.LogWarning($"ENV '{fallbackEnvVarName}' not set");

            return envNs;
        }
    }
}