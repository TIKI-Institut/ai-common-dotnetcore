using System.Diagnostics.CodeAnalysis;
using Newtonsoft.Json;

namespace AiCommon.Common.Kubernetes
{
    public interface IPodReference
    {
        string Namespace { get; }
        string Pod { get; }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class PodReference : IPodReference
    {
        // only for data access
        [JsonIgnore] public int Id { get; set; }

        [JsonProperty("namespace")] public string Namespace { get; set; }

        [JsonProperty("pod")] public string Pod { get; set; }

        protected bool Equals(PodReference other)
        {
            return string.Equals(Namespace, other.Namespace) && string.Equals(Pod, other.Pod);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((PodReference) obj);
        }

        [SuppressMessage("ReSharper", "NonReadonlyMemberInGetHashCode")]
        public override int GetHashCode()
        {
            unchecked
            {
                return ((Namespace != null ? Namespace.GetHashCode() : 0) * 397) ^
                       (Pod != null ? Pod.GetHashCode() : 0);
            }
        }

        public override string ToString() => $"{(string.IsNullOrWhiteSpace(Namespace) ? "<unset>" : Namespace)}/{(string.IsNullOrWhiteSpace(Pod) ? "<unset>" : Pod)}";

        public static PodReference FromPodReference(IPodReference @ref)
        {
            return new PodReference
            {
                Namespace = @ref.Namespace,
                Pod = @ref.Pod
            };
        }
    }
}